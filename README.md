<img src="https://i.ibb.co/3pDD8dX/dnetdxracerslogo.png" alt="logo-dnetdxracers" width="300px" style="margin-top: 20px;"/>

### dNet.dXracers

Official repository of the dNet.dXracers team.

Team members: Stoyan Kuklev, Yana Staneva

<a href=https://trello.com/b/HIXaDhSi/deliverit>Link to Trello board</a>

## DeliverIT 

# Description & User Roles

This project is an ASP.NET Core web application, aiming to serve the business requirements and needs of a <a href=https://en.wikipedia.org/wiki/Freight_forwarder>freight forwarding company</a>. The system supports three authorization roles, namely:
1. Guest users - viewers, who can only view and print publicly available items.
2. Registered users (_Customers_) - viewers, who can view and print privately available items upon authentication.
3. Administrators (_Employees_) - users, who can create, read, update and delete various entities upon authentication.

# Getting Strated with DeliverIT

In order to build and run the project, follow these steps:

1. Open <a href="https://visualstudio.microsoft.com/">Microsoft Visual Studio</a> and follow these <a href="https://docs.microsoft.com/en-us/visualstudio/get-started/tutorial-open-project-from-repo-visual-studio-2019?view=vs-2019&tabs=vs168later">Instructions for Cloning a Repo</a>
2. Paste the link of the DeliverIT repo: https://gitlab.com/cinderglacier/deliverit.git 
3. When the cloning is ready, you should be able to see the following items in the Solution Explorer ![Solution Explorer](https://gitlab.com/cinderglacier/deliverit/-/raw/master/requirements/solutionExplorer.PNG)
4. At the top of the screen, your main menu should look like this: ![Main Menu](https://gitlab.com/cinderglacier/deliverit/-/raw/master/requirements/menu.PNG). Make sure to have the **DeliverIT.Web** Startup Project selected. Then hit the green triangle button ![Button](https://gitlab.com/cinderglacier/deliverit/-/raw/master/requirements/button.PNG).
5. If everything worked out fine, the Swagger API documentation should open in your main browser:
![Swagger](https://gitlab.com/cinderglacier/deliverit/-/raw/master/requirements/swagger.PNG)

# Web, HTTP, REST API

DeliverIT is built via the **Client-Server Model**, where the communication in the form of the **Request/Response model** is established via **HTTP** (<a href=https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol>HyperText Transfer Protocol</a>). 

The exchange of data is naturally in **JSON** text format. However, there are **no DTOs** (data transfer objects) included, so the returned objects could be quite extensive and unnecessarily detailed at times. 

All valid **URLs** (<a href = https://en.wikipedia.org/wiki/URL>uniform resource locators</a>) shall return _Successful (2xx)_ or _Client Error (4xx)_ status codes when the request was successully or unsuccessfully processed, respectively.

The DeliverIT application provides a **RESTful API** (Application Programming Interface, following the <a href=https://en.wikipedia.org/wiki/Representational_state_transfer>REpresentational State Transfer</a> architechtural standard), in order to conform to the decoupled server and client development and maintenance practices. All HTTP request methods for a given rersource are implemented in REST as usual:
1. GET - to view a representation of the resource,
2. POST - to create an instance of the resource,
3. PUT - to update the properties of the resource,
4. DELETE - to virtually remove the resource.

Note that by "virtually remove" a resource, we do not mean a removal of the resource from the database. That is why the entities are equipped with a boolean _IsDeleted_ property, which is set to _true_ on calling the DELETE request.

All endppoints follow the basic nomenclature standards with 
- nouns in the URLs,
- sub-resourcing,
- query parameters for filtering, sorting and searching, where available.

Due to the scope of the project, there is a unique API version.

# ASP.NET Core Structure

DeliverIT is **not** connected to any outer services, such as <a href=https://azure.microsoft.com/en-us/>_Azure_</a>.

The used _Analyzers_ are:
- Microsoft.AspNetCore.Analyzers 
- Microsoft.AspNetCore.Components.Analyzers
- Microsoft.AspNetCore.Mvc.Analyzers  
- Microsoft.EntityFrameworkCore.Analyzers

The used _Frameworks_ are:
- Microsoft.AspNetCore.App 
- Microsoft.NETCore.App 

The included _Packages_ are:
- <a href = https://www.nuget.org/packages/Microsoft.EntityFrameworkCore.Design/>Microsoft.EntityFrameworkCore.Design</a> - used for the database setup, design and management,
- <a href = https://www.nuget.org/packages/Swashbuckle.AspNetCore/> Swashbuckle.AspNetCore </a> - the <a href="https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-5.0&tabs=visual-studio">Swagger</a> tools, used for documenting the API. 

The compile time properties are setup in the **launchSettings.json** file. The application is configured to launch with the **DeliverIT** profile, where the **"launchUrl" = "swagger"** and **"applicationUrl" = "https://localhost:5000"**.

The application entry point is located in the **Program.cs** file. 

# Layers

Following the best <a href=https://en.wikipedia.org/wiki/Multitier_architecture>Layered Acrhitecture</a> principles for maximum **decoupling**, **maintainability**, **extensibility** and **testability**, the DeliverIT application is split into 3 main layers as follows:
1. Presentation Layer - DeliverIT.Web
2. Business Logic Layer - DeliverIT.Services
3. Data Access Layer - DeliverIT.Data

There are 2 more libraries included in the application for error handling and testing purposes:
- Custom Exceptions Library - DeliverIT.Exceptions
- Unit Testing Library - DeliverIT.Tests

# IoC Container

According to the <a href=https://en.wikipedia.org/wiki/Dependency_inversion_principle>**Dependency Inversion Principle**</a>,
- "High-level modules should not depend on low-level modules. Both should depend on abstractions."
- "Abstractions should not depend on details. Details should depend on abstractions." 

Following the best practices of Object Oriented Programming, DeliverIT has an **IoC Container** (Inversion of Control Container) where all the project dependencies are registered and managed.

The IoC container is the <a href="https://docs.microsoft.com/en-us/aspnet/core/fundamentals/dependency-injection?view=aspnetcore-5.0">ASP.NET Core built-in dependency injection container</a> located in the application composition root **Startup.cs** in the DeliverIT.Web layer. All services are registered in the _Startup.ConfigureServices_ method and have scoped lifetimes, so that new instances of the services are created for every scope.

# Entity Framework Core & Database

DeliverIT utilizes **EF** (<a href=https://docs.microsoft.com/en-us/ef/>Entity Framework</a>) - the standard **ORM** (<a href="https://en.wikipedia.org/wiki/Object%E2%80%93relational_mapping">Object-Relational Mapping</a>) framework for .NET applications in order to map the CLR objects to tables in the <a href=https://en.wikipedia.org/wiki/SQL>SQL</a> database.

We have followed the _Code First Approach_ to build the DeliverIT relational database. The following models are the building blocks of the application and are located in the **DeliverIT.Data** layer:
* Address
* Category
* City
* Country
* Customer 
* Employee 
* Parcel
* Shipment
* Status
* Warehouse

The implementation of the models follows the <a href=https://en.wikipedia.org/wiki/Object-oriented_programming>OOP</a> paradigm to the best of our abilities.

The properties of each objects and the relations between the objects are set within each class via the attributes provided by the _System.ComponentModel.DataAnnotations_ namespace and are illustrated in the diagram below.

![SQL Diagram](https://gitlab.com/cinderglacier/deliverit/-/raw/master/requirements/sqlDiagram.PNG)

The communication between the DeliverIT web application and the relational SQL database is established via the EF <a href="https://docs.microsoft.com/en-us/dotnet/api/system.data.entity.dbcontext?view=entity-framework-6.2.0">DbContext</a> API. The required _Packages_ are:
- Microsot.EntityFrameworkCore.SqlServer - used to establish connection between our application and the SQL Server 
- Microsoft.EntityFrameworkCore.Tools - necessary for EF to create the relational database based on the models in the **DeliverIT.Data** project.

The initial seeding of the database is established in the **DeliverITContext.cs**, which inherits the **DbContext.cs**.

In order to setup the database locally, first make sure that you have <a href = "https://www.sqlshack.com/how-to-install-sql-server-express-edition/">SQL Server</a> and <a href="https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-ver15">SQL Server Management Studio</a> installed and that the server is <a href=https://www.mssqltips.com/sqlservertip/6307/how-to-stop-and-start-sql-server-services/>running</a>. 

Then, please open your **Package Manager Console** <a href=https://docs.microsoft.com/en-us/nuget/consume-packages/install-use-packages-powershell>(Tools => NuGet Package Manager => Package Manager Console)</a> and set the _Default Project_ to **DeliverIT.Data**. Then enter the following commands in the console:
1. add-migration initial
2. update-database

# Service Layer

The business logic for the freight forwarding management system as per the <a href=requirements/FreightForwardingManagementSystem.pdf>Technical Requirements</a> is implemented in the **DeliverIT.Services** project. The main principles we have strived to adhere to are the <a href=https://en.wikipedia.org/wiki/KISS_principle>KISS</a>, <a href=https://en.wikipedia.org/wiki/SOLID>SOLID</a> and <a href=https://en.wikipedia.org/wiki/Don%27t_repeat_yourself>DRY</a> ones. Each service has a corresponding _interface_, used to leverage abstraction.

# Authentication

Registered users authenticate via email. Authentication is achieved within the controllers via HTTP headers and through the helper **AuthHelper.cs**, which queries the Customer/Employee databases via the respective services.

Authorization is granted according to the <a href=requirements/FreightForwardingManagementSystem.pdf>Technical Requirements</a>.

1. Public part - Guest users:
* Can see how many customers DeliverIT has
* Can see available warehouses
* Can register

2. Private Part - a) Registered users (_Customers_):
* Can see their inactive and active parcels
* Can see the status of the shipment which holds a parcel of theirs

b) Registered users (_Employees_):
* Can create, update and delete warehouses
* Can create, update, delete, filter, sort parcels
* Can add and remove parcels from a shipment
* Can see all shipments and filter them by status
* Can see the next shipment to arrive to a specific warehouses

3. Administrative part - Administrators (_Employees_):
* Can see and manage all entities in the system, including all customers
* Can search and filter by multiple criteria
