﻿using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Exceptions;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DeliverIT.Data;
using Microsoft.AspNetCore.Http;

namespace DeliverIT.WIndividualAccounts.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login (string email)
        {
            //TODO : Authenticate against database

            if (string.IsNullOrEmpty(email))
            {
                return View("Shared/Error");
            }

            var user = this.userService.GetByEmail(email);
            if (user == null)
                return Unauthorized();

            this.HttpContext.Session.SetString("user", user.Email);
            this.HttpContext.Session.SetString("role", user.Role.Name);

            //TODO : Load email & role to session 

            return RedirectToAction("Profile");
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View();
        }

    }
}
