﻿using DeliverIT.Data.Models;

namespace DeliverIT.Web.Helpers
{
    public interface IAuthHelper
    {
        Employee GetValidEmployee(string email);
        Customer GetValidCustomer(string email);

        bool DoesEmailBelongToCustomerId(string email, int customerId);
        bool DoesEmailBelongToEmployeeId(string email, int employeeId);
    }
}
