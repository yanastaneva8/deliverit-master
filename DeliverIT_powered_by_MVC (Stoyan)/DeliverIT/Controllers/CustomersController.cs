﻿using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace DeliverIT.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CustomersController : ControllerBase
    {
        /// <summary>
        /// Customer service and auth helper fields
        /// </summary>
        private readonly ICustomerService customerService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with customer service and auth helper injection
        /// </summary>
        /// <param name="customerService">Customer service</param>
        /// <param name="authHelper">Authentication helper</param>
        public CustomersController(ICustomerService customerService, 
            IAuthHelper authHelper)
        {
            this.customerService = customerService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get customer by Id
        /// </summary>
        /// <param name="id">Customer Id </param>
        /// <param name="email">Email for authorization</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null
                    || this.authHelper.DoesEmailBelongToCustomerId(email, id))
                {
                    return Ok(customerService.Get(id));
                }

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get users by criteria, if no parameters are specified, get all users
        /// </summary>
        /// <param name="firstName">First name - optional</param>
        /// <param name="lastName">Last name - optional</param>
        /// <param name="email">Email - optional</param>
        /// <param name="empEmail">Employee email for authentication</param>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Get([FromHeader] string empEmail,
            [FromQuery] string firstName = null,
            string lastName = null,
            string email = null)
        {
            if (this.authHelper.GetValidEmployee(empEmail) != null)
            {
                var customers = customerService.Get(firstName, lastName, email);

                if (customers.Count() == 0)
                {
                    return this.NotFound(ConstantStringMessages.customerNotFound);
                }
                    
                return this.Ok(customers);
            }
            return Unauthorized(ConstantStringMessages.unauthorized);

        }


        /// <summary>
        /// Get number of customers
        /// </summary>
        /// <returns></returns>
        [HttpGet("number")]
        public IActionResult GetNumber()
        {
            return this.Ok(customerService.GetTotalNumber());
        }

        /// <summary>
        /// Create new customer
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="deliveryWarehouseId">Delivry warehouse Id</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post([FromQuery] string firstName,
            string lastName,
            string email,
            int deliveryWarehouseId)
        {
            try
            {
                var customer = customerService.Post(firstName, lastName, email, deliveryWarehouseId);
                return this.Created("post", customer);
            }
            catch (NonExistentId e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete(int id, [FromHeader] string email)
        {
            if (this.authHelper.GetValidEmployee(email) != null
                    || this.authHelper.DoesEmailBelongToCustomerId(email, id))
            {
                var result = customerService.Delete(id);

                if (result)
                    return this.Ok(ConstantStringMessages.customerDeleted);

                return this.NotFound(ConstantStringMessages.customerNotFound);
            }

            return Unauthorized(ConstantStringMessages.unauthorized);

        }

        /// <summary>
        /// Update customer details
        /// </summary>
        /// <param name="headerMail">Email for authentication</param>
        /// <param name="id">Customer Id</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="deliveryWarehouseId">Delivery warehouse Id</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string headerMail,
            int id,
            [FromQuery] string firstName = null,
            string lastName = null,
            string email = null,
            int deliveryWarehouseId = 0)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(headerMail) != null
                    || this.authHelper.DoesEmailBelongToCustomerId(headerMail, id))
                {
                    return this.Ok(customerService.Put(id, firstName, lastName, email, deliveryWarehouseId));
                }
                return Unauthorized();
            }
            catch (NonExistentId e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets parcels of a customer based on a criteria. Input accepts preparing/ontheway/completed/collected as criteria.
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="criteria">Criteria - preparing/ontheway/completed/collected</param>
        /// <param name="id">Id of customer</param>
        /// <returns>List of parcels belonging to customer based on the criteria</returns>
        [HttpGet("{id}/parcels")]
        public IActionResult GetCustomerIncomingParcels([FromHeader] string email, 
            [FromQuery] string criteria, 
            int id)
        {
            
            if(this.authHelper.GetValidEmployee(email) != null 
                || this.authHelper.DoesEmailBelongToCustomerId(email, id))
            {
                try
                {
                    var parcels = this.customerService.GetParcelsOfCustomerByCriteria(id, criteria);
                    if (parcels.Count() == 0)
                        return this.NotFound(ConstantStringMessages.parcelNotFound);

                    return this.Ok(parcels);
                }
                catch (InvalidQueryCriteria e)
                {
                    return this.BadRequest(e.Message);
                }
                
            }
            return Unauthorized(ConstantStringMessages.unauthorized);

        }

        /// <summary>
        /// Search customer by keyword
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="seeker">Seeker keyword</param>
        /// <returns></returns>
        [HttpGet("searchallprops")]
        public IActionResult Search([FromHeader] string email,
            [FromQuery] string seeker)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                if (seeker == null)
                    return this.BadRequest(ConstantStringMessages.criteriaNotSpecified);

                var customers = customerService.Search(seeker);

                if (customers.Count() == 0)
                    return this.NotFound(ConstantStringMessages.customerNotFound);

                return this.Ok(customers);
            }

            return Unauthorized(ConstantStringMessages.unauthorized);

        }

    }
}
