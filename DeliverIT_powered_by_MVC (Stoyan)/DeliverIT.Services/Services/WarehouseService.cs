﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
     public class WarehouseService : IWarehouseService
    {
        /// <summary>
        /// Db and address service fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly IAddressService addressService;

        
        /// <summary>
        /// Ctor with db and address service injection
        /// </summary>
        /// <param name="context"></param>
        /// <param name="addressService"></param>
        public WarehouseService(DeliverITContext context, 
            IAddressService addressService)
        {
            this.context = context;
            this.addressService = addressService;
        }

        /// <summary>
        /// Get warehouse by Id
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public Warehouse Get(int id)
        {
            var warehouse = context.Warehouses
                .Where(w=>!w.IsDeleted)
                .Include(w=>w.Address)
                    .ThenInclude(a=>a.City)
                    .ThenInclude(a=>a.Country)
                .FirstOrDefault(w => w.WarehouseId == id)
                ?? throw new NonExistentId("warehouse", id);

            return warehouse;
        }

        /// <summary>
        /// Get next shipment to arrive in warehouse
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public Shipment GetNextShipment(int id)
        {
            var shipment = context.Shipments
                .Where(s => s.ArrivalWarehouseId == id)
                .Where(s => s.StatusId == 2)
                .OrderByDescending(s => s.ArrivalDate)
                .FirstOrDefault()
                ?? throw new NoPendingItems("shipments");

            return shipment;
        }

        /// <summary>
        /// Get warehouses
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Warehouse> Get()
        {
            var warehouses = context.Warehouses
                .Where(w=>!w.IsDeleted)
                .Include(w=>w.Address)
                    .ThenInclude(a=>a.City)
                    .ThenInclude(c=>c.Country);

            return warehouses;
        }
        

        
        /// <summary>
        /// Create new warehouse
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <returns></returns>
        public Warehouse Post(string cityName, 
            string countryName, 
            string streetName)
        {
            var warehouse = new Warehouse();

            var address = addressService.Post(cityName, countryName, streetName);
            warehouse.AddressId = address.AddressId;
            warehouse.Address = address;

            var newWarehouse = this.context.Warehouses.Add(warehouse);
            context.SaveChanges();

            return newWarehouse.Entity;
        }

        /// <summary>
        /// Update warehouse properties
        /// </summary>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <param name="newCountry">New country</param>
        /// <param name="newCity">New city</param>
        /// <param name="newStreetName">New street name</param>
        /// <returns></returns>
        public Warehouse Put(int warehouseId, 
            string newCountry, 
            string newCity, 
            string newStreetName)
        {
            var warehouse = this.Get(warehouseId);

            warehouse.Address = addressService.Put(warehouse.AddressId, newCountry, newCity, newStreetName);

            context.SaveChanges();
            return warehouse;
        }

        /// <summary>
        /// Delete warehouse
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            var warehouse = this.Get(id);
            if (warehouse == null)
                return false;

            warehouse.IsDeleted = true;

            context.SaveChanges();
            return true;
        }
    }
}
