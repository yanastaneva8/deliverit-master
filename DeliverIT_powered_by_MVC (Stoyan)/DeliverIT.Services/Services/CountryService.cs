﻿using System.Collections.Generic;
using System.Linq;
using DeliverIT.Data.Models;
using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Exceptions;

namespace DeliverIT.Services.Services
{
    public class CountryService : ICountryService
    {
        /// <summary>
        /// Db field
        /// </summary>
        private readonly DeliverITContext context;


        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context"></param>
        public CountryService(DeliverITContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get country by id
        /// </summary>
        /// <param name="id">Country id</param>
        /// <returns>Country</returns>
        public Country Get(int id)
        {
            var country = context.Countries
                .FirstOrDefault(c => c.CountryId == id)
                ?? throw new NonExistentId("country", id);

            return country;
        }

        /// <summary>
        /// Get country by name
        /// </summary>
        /// <param name="countryName">Country name</param>
        /// <returns></returns>
        public Country Get(string countryName)
        {
            return context.Countries.FirstOrDefault(co => co.Name == countryName)
                ?? throw new NonExistentName("country", countryName);
        }
        
        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns></returns>
        public IEnumerable<Country> GetAll()
        {
            var countries = context.Countries;

            return countries;
        }
    }

           
}
