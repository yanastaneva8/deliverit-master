﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services.Services
{
    public class AddressService : IAddressService
    {
        /// <summary>
        /// Db, city and country services fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;


        /// <summary>
        /// Ctor with db, city & country services injections
        /// </summary>
        /// <param name="deliverITContext"></param>
        /// <param name="cityService"></param>
        /// <param name="countryService"></param>
        public AddressService(DeliverITContext deliverITContext, 
            ICityService cityService, 
            ICountryService countryService)
        {
            this.context = deliverITContext;
            this.cityService = cityService;
            this.countryService = countryService;
        }

        /// <summary>
        /// Get all addresses
        /// </summary>
        /// <returns>IEnumerable of addresses</returns>
        public IEnumerable<Address> Get()
        {
            var addresses = this.context.Addresses
                .Include(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(a => a.Country);

            return addresses;
        }

        /// <summary>
        /// Get address by id
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <returns>Address</returns>
        public Address Get(int id)
        {
            var address = this.context.Addresses
                 .Include(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(a => a.Country)
                .FirstOrDefault(a => a.AddressId == id)
                ?? throw new NonExistentId("address", id);

            return address;
        }

        /// <summary>
        /// Post method to create address
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <returns>Address</returns>
        public Address Post(string cityName, 
            string countryName, 
            string streetName)
        {
            var address = new Address();

            var country = this.countryService.Get(countryName);
            address.CountryId = country.CountryId;
            address.Country = country;

            var city = this.cityService.Get(cityName)
                ?? this.cityService.Post(cityName, countryName);
            address.CityId = city.CityId;
            address.City = city;

            address.StreetName = streetName;

            var newAddress = context.Addresses.Add(address);
            context.SaveChanges();

            return newAddress.Entity;
        }
        
        /// <summary>
        /// Put method to update address
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <param name="countryName">Country name</param>
        /// <param name="cityName">City name</param>
        /// <param name="streetName">Street name</param>
        /// <returns>Address</returns>
        public Address Put(int id, 
            string countryName, 
            string cityName, 
            string streetName)
        {
            var address = this.Get(id);

            if (countryName != null)
            {
                var country = countryService.Get(countryName);
                address.CountryId = country.CountryId;
                address.Country = country;
            }

            if (cityName != null)
            {
                var city = cityService.Get(cityName)
                ?? cityService.Post(cityName, countryName);
                address.CityId = city.CityId;
                address.City = city;
            }

            if (address.City.CountryID != address.Country.CountryId)
                throw new UnsynchronizedIds(address.Country.Name, address.City.Name, address.Country.CountryId);

            if (streetName != null)
            {
                address.StreetName = streetName;
            }

            context.SaveChanges();
            return address;

        }

    }
}