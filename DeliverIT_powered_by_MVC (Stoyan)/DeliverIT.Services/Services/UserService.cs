﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Services
{
    public class UserService : IUserService
    {
        private readonly DeliverITContext context;

        public UserService(DeliverITContext context)
        {
            this.context = context;
        }

        public User GetByEmail(string email)
        {
            var userDrawer = new List<User>();
            userDrawer.Add(this.context.Customers.Include(cu=>cu.Role).FirstOrDefault(cu => cu.Email == email));
            userDrawer.Add(this.context.Employees.Include(em => em.Role).FirstOrDefault(em => em.Email == email));
            return userDrawer.FirstOrDefault(us=>us!=null);
        }


    }
}
