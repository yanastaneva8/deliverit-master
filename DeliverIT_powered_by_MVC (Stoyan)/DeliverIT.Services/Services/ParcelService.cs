﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Exceptions;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using System;

namespace DeliverIT.Services.Services
{
    public class ParcelService : IParcelService
    {
        /// <summary>
        /// Database context field, warehouse, and customer service
        /// </summary>
        private readonly DeliverITContext context;
        private readonly IWarehouseService warehouseService;
        private readonly ICustomerService customerService;
        private readonly IShipmentService shipmentService;


        /// <summary>
        /// Ctor with db injection
        /// </summary>
        /// <param name="context"></param>
        public ParcelService(DeliverITContext context,
            IWarehouseService warehouseService,
            ICustomerService customerService,
            IShipmentService shipmentService)
        {
            this.context = context;
            this.warehouseService = warehouseService;
            this.customerService = customerService;
            this.shipmentService = shipmentService;
        }

        /// <summary>
        /// Get method for parcels
        /// </summary>
        /// <param name="weight">Weight</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="arrivalWarehouseId">Arrival warehouse Id</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="categoryId">Category Id</param>
        /// <param name="sortingCriteria">Sorting criteria - w (weight), ad (arrival date), wad (both)</param>
        /// <param name="sortingOrder">Sorting order - a (ascending), d (descending)</param>
        /// <returns>IQueryable of parcels</returns>
        public IQueryable<Parcel> Get(
            double weight,
            int customerId,
            int arrivalWarehouseId,
            int departureWarehouseId,
            int categoryId,
            string sortingCriteria,
            string sortingOrder)
        {
            // Initialize parcels variable
            var parcels = this.context.Parcels
                .Where(p => !p.IsDeleted)
                .Include(p => p.Customer)
                    .ThenInclude(c => c.DeliveryWarehouse)
                    .ThenInclude(d => d.Address)
                .Include(p => p.ArrivalWarehouse)
                    .ThenInclude(w => w.Address)
                .Include(p => p.DepartureWarehouse)
                    .ThenInclude(w => w.Address)
                .Include(p => p.Category)
                .Include(p => p.Shipment)
                .AsQueryable();

            // Filter by weight
            if (weight != default)
            {
                parcels = parcels.Where(p => p.Weight == weight);
            }

            // Filter by customer
            if (customerId != default)
            {
                parcels = parcels.Where(p => p.CustomerId == customerId);
            }

            // Filter by arrival warehouse Id
            if (arrivalWarehouseId != default)
            {
                parcels = parcels.Where(p => p.ArrivalWarehouseId == arrivalWarehouseId);
            }

            // Filter by departure warehouse Id
            if (departureWarehouseId != default)
            {
                parcels = parcels.Where(p => p.DepartureWarehouseId == departureWarehouseId);
            }

            // Filter by category
            if (categoryId != default)
            {
                parcels = parcels.Where(p => p.CategoryId == categoryId);
            }

            // Sorting
            if (sortingCriteria != default)
            {
                switch (sortingCriteria)
                {
                    case ("w"):
                        switch (sortingOrder)
                        {
                            case ("d"):
                                parcels = parcels.OrderByDescending(p => p.Weight);
                                break;
                            case ("a"):
                                parcels = parcels.OrderBy(p => p.Weight);
                                break;
                            default:
                                parcels = parcels.OrderBy(p => p.Weight);
                                break;
                        }
                        break;
                    case ("ad"):
                        switch (sortingOrder)
                        {
                            case ("d"):
                                parcels = parcels.OrderByDescending(p => p.Shipment.ArrivalDate);
                                break;
                            case ("a"):
                                parcels = parcels.OrderBy(p => p.Shipment.ArrivalDate);
                                break;
                            default:
                                parcels = parcels.OrderBy(p => p.Shipment.ArrivalDate);
                                break;
                        }
                        break;
                    case ("wad"):
                        switch (sortingOrder)
                        {
                            case ("d"):
                                parcels = parcels.OrderByDescending(p => p.Weight)
                                    .ThenByDescending(p => p.Shipment.ArrivalDate);
                                break;
                            case ("a"):
                                parcels = parcels.OrderBy(p => p.Weight)
                                    .ThenBy(p => p.Shipment.ArrivalDate);
                                break;
                            default:
                                parcels = parcels.OrderBy(p => p.Weight)
                               .ThenBy(p => p.Shipment.ArrivalDate);
                                break;
                        }
                        break;
                    default:
                        break;
                }
            }

            return parcels;
        }

        /// <summary>
        /// Get parcel by Id
        /// </summary>
        /// <returns>Parcel</returns>
        public Parcel Get(int id)
        {
            // Initialize parcels variable
            var parcels = this.context.Parcels
                .Where(p => !p.IsDeleted)
                .Include(p => p.Customer)
                    .ThenInclude(c => c.DeliveryWarehouse)
                    .ThenInclude(d => d.Address)
                .Include(p => p.ArrivalWarehouse)
                    .ThenInclude(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .Include(p => p.DepartureWarehouse)
                    .ThenInclude(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .Include(p => p.Category)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.ArrivalWarehouse)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.DepartureWarehouse)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                .AsQueryable();

            // Find required parcel
            var parcel = parcels
                .FirstOrDefault(p => p.ParcelId == id)
                ?? throw new NonExistentId("parcel", id);

            return parcel;
        }

        /// <summary>
        /// Create parcel
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="shipmentId">Shipment Id</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="arrivalWarehouseId">Arrival warehouse Id</param>
        /// <param name="weight">Weight</param>
        /// <param name="categoryId">Category Id</param>
        /// <returns>Parcel</returns>
        public Parcel Post(int customerId,
            int departureWarehouseId,
            int arrivalWarehouseId,
            double weight,
            int categoryId,
            int? shipmentId)
        {
            var parcel = new Parcel();

            // Query db for customer
            var customer = customerService.Get(customerId);

            // Query db for departure warehouse
            var departureWarehouse = warehouseService.Get(departureWarehouseId);

            // Query db for arrival warehouse
            var arrivalWarehouse = warehouseService.Get(arrivalWarehouseId);

            Shipment shipment;
            // Query db for shipment
            if (shipmentId != default)
            {
                shipment = shipmentService.Get(Convert.ToInt32(shipmentId));
                parcel.ShipmentId = shipmentId;
                parcel.Shipment = shipment;
            }


            // Set properties
            parcel.ArrivalWarehouseId = arrivalWarehouse.WarehouseId;
            parcel.ArrivalWarehouse = arrivalWarehouse;
            parcel.CategoryId = categoryId;
            parcel.Category = context.Categories.FirstOrDefault(c => c.CategoryId == categoryId);
            parcel.CustomerId = customer.CustomerId;
            parcel.Customer = customer;
            parcel.DepartureWarehouseId = departureWarehouseId;
            parcel.DepartureWarehouse = departureWarehouse;
            parcel.Weight = weight;

            // Add to db
            var newParcel = this.context.Parcels.Add(parcel);
            context.SaveChanges();

            return newParcel.Entity;
        }

        /// <summary>
        /// Update parcel
        /// </summary>
        /// <param name="parcelId">Parcel Id</param>
        /// <param name="newArrivalWarehouseId">New arrival warehouse Id</param>
        /// <param name="newDepartureWarehouseId">New departure warehouse Id</param>
        /// <returns>Parcel</returns>
        public Parcel Put(int parcelId,
            int newArrivalWarehouseId,
            int newDepartureWarehouseId,
            int newShipmentId)
        {


            // Find required parcel
            var parcel = this.Get(parcelId);

            // Query db for arrival warehouse
            if (newArrivalWarehouseId != default)
            {
                var arrivalWarehouse = warehouseService.Get(newArrivalWarehouseId);
                parcel.ArrivalWarehouseId = arrivalWarehouse.WarehouseId;
                parcel.ArrivalWarehouse = arrivalWarehouse;
                parcel.ArrivalWarehouse.Address = arrivalWarehouse.Address;
                parcel.ArrivalWarehouse.WarehouseId = arrivalWarehouse.WarehouseId;
            }
            // Query db for departure warehouse
            if (newDepartureWarehouseId != default)
            {
                var departureWarehouse = warehouseService.Get(newDepartureWarehouseId);
                parcel.DepartureWarehouseId = departureWarehouse.WarehouseId;
                parcel.DepartureWarehouse = departureWarehouse;
                parcel.DepartureWarehouse.Address = departureWarehouse.Address;
                parcel.DepartureWarehouse.WarehouseId = departureWarehouse.WarehouseId;
            }

            // Query shipments
            if (newShipmentId != default)
            {
                var shipment = shipmentService.Get(newShipmentId);
                parcel.Shipment = shipment;
                parcel.ShipmentId = shipment.ShipmentId;
            }


            // Update db
            context.SaveChanges();

            return parcel;

        }

        /// <summary>
        /// Gets the status of a shipment holding a parcel with this id
        /// </summary>
        /// <param name="parcelId">Parcel Id</param>
        /// <returns>The status of the shipment if valid</returns>
        public Status GetStatusOfShipmentHoldingParcel(int parcelId, int customerId)
        {
            var parcel = this.Get(parcelId);

            if (parcel.CustomerId != customerId)
                throw new ParcelDoesNotBelongToCustomer(parcelId, customerId);
            var shipment = parcel.Shipment ?? throw new ParcelNotAssignedToShipment(parcelId);
            var status = shipment.Status;


            return status;
        }


        /// <summary>
        /// Delete method
        /// </summary>
        /// <param name="id">Parcel Id</param>
        /// <returns>True if deletion is successful, false - if not</returns>
        public bool Delete(int id)
        {
            // Query db for parcel
            try
            {
                var parcel = this.Get(id);

                parcel.IsDeleted = true;

                // Update db
                context.SaveChanges();

                return true;
            }
            catch (NonExistentId)
            {
                return false;
            }

        }
    }
}
