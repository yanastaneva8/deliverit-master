﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace DeliverIT.Services.Services
{



    public class CityService : ICityService
    {
        /// <summary>
        /// Db and country service fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly ICountryService countryService;


        /// <summary>
        /// Ctor with db and country service injections
        /// </summary>
        /// <param name="context"></param>
        /// <param name="countryService"></param>
        public CityService(DeliverITContext context,
            ICountryService countryService)
        {
            this.context = context;
            this.countryService = countryService;
        }

        /// <summary>
        /// Get city by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public City Get(int id)
        {
            var city = context.Cities
                .Include(ci=>ci.Country)
                .FirstOrDefault(ci => ci.CityId == id)
                ?? throw new NonExistentId("city", id);


            return city;
        }

        /// <summary>
        /// Get city by name
        /// </summary>
        /// <param name="cityName"></param>
        /// <returns></returns>
        public City Get(string cityName)
        {
            var city = context.Cities
                .Include(c => c.Country)
                .FirstOrDefault(ci => ci.Name == cityName)
                ?? throw new NonExistentName("city", cityName);

            return city;
        }

        /// <summary>
        /// Get all cities
        /// </summary>
        /// <returns></returns>
        public IEnumerable<City> Get()
        {
            var cities = this.context.Cities
                .Include(ci=>ci.Country);

            return cities;
        }
        
        /// <summary>
        /// Create new city
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <returns></returns>
        public City Post(string cityName, string countryName)
        {
            var city = new City();
            
            city.Name = context.Cities.Any(ci => ci.Name == cityName) 
                ? throw new AlreadyExisting("city", cityName) 
                : cityName;

            var country = countryService.Get(countryName);
            city.CountryID = country.CountryId;
            city.Country = country;

            var newCity = context.Cities.Add(city);
            
            context.SaveChanges();
            return newCity.Entity;
        }

    }
}
