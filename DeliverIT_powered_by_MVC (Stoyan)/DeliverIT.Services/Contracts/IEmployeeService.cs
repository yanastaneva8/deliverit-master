﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IEmployeeService
    {
        Employee Get(int id);
        IQueryable<Employee> Get(string firstNameQr, string lastNameQr, string emailQr);
        Employee Post(string firstName, string lastName, string email, int warehouseId);
        bool Delete(int id);
        Employee Put(int id, string firstName, string lastName, string email, int warehouseId);
        IEnumerable<Employee> Search(string seeker);
        Employee GetByEmail(string email);
    }
}
