﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface ICustomerService
    {
        Customer Get(int id);
        IQueryable<Customer> Get(string firstNameQr, string lastNameQr, string emailQr);
        Customer GetByEmail(string email);
        int GetTotalNumber();
        Customer Post(string firstName, string lastName, string email, int deliveryWarehouseId);
        bool Delete(int id);
        Customer Put(int id, string firstName, string lastName, string email, int deliveryWarehouseId);
        IQueryable<Parcel> GetParcelsOfCustomerByCriteria(int customerId, string deliveryStatusCriteria);
        IEnumerable<Customer> Search(string seeker);
    }
}
