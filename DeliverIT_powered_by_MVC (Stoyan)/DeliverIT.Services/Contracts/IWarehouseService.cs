﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IWarehouseService
    {
        Warehouse Get(int id);

        IEnumerable<Warehouse> Get();

        Shipment GetNextShipment(int id);

        Warehouse Post(string cityName, string countryName, string streetName);

        public Warehouse Put(int warehouseId, string newCountry, string newCity, string newStreetName);

        bool Delete(int id);
    }
}
