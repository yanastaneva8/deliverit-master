﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IParcelService
    {
        public Parcel Get(int id);
        public IQueryable<Parcel> Get(double weight,
            int customerId,
            int arrivalWarehouseId,
            int departureWarehouseId,
            int categoryId,
            string sortingCriteria,
            string sortingOrder);
        public Status GetStatusOfShipmentHoldingParcel(int parcelId, int customerId);
        public Parcel Post(int customerId,
            int departureWarehouseId,
            int arrivalWarehouseId,
            double weight,
            int categoryId,
            int? shipmentId);
        public Parcel Put(int parcelId,
            int newArrivalWarehouseId,
            int newDepartureWarehouseId,
            int newShipmentId);
        public bool Delete(int id);
    }
}
