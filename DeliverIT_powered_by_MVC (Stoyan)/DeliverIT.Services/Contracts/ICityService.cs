﻿using DeliverIT.Data.Models;
using System.Collections.Generic;


namespace DeliverIT.Services.Contracts
{
    public interface ICityService
    {
        City Get(int id);

        City Get(string cityName);

        IEnumerable<City> Get();

        City Post(string cityName, string countryName);
    }
}
