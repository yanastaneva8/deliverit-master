﻿using DeliverIT.Data.Models;

namespace DeliverIT.Services.Contracts
{
    public interface IUserService
    {
        User GetByEmail(string email);
    }
}