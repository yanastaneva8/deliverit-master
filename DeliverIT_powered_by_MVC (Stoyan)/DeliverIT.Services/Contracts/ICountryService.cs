﻿using DeliverIT.Data.Models;
using System.Collections.Generic;


namespace DeliverIT.Services.Contracts
{
    public interface ICountryService
    {
        Country Get(int id);

        Country Get(string countryName);

        IEnumerable<Country> GetAll();
    }
}
