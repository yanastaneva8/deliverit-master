﻿using DeliverIT.Data.Models;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Contracts
{
    public interface IShipmentService
    {
        public IQueryable<Shipment> GetMany(int? customerId);

        public Shipment Get(int id);

        public Shipment Post(string departureDateString,
            string arrivalDateString,
            int departureWarehouseId,
            int arrivalWarehouseId);

        public void AdvanceStatus(int shipmentId);

        public void RevertStatus(int shipmentId);

        public void DispatchShipment(int shipmentId,
            int travelTimeHrs);

        public void ReceiveShipment(int shipmentId);
        public List<Parcel> AddParcelsToShipment(int shipmentId, 
            params int[] parcelIds);

        public bool Delete(int shipmentId);
    }
}
