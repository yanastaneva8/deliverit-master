﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IAddressService
    {
        IEnumerable<Address> Get();

        Address Get(int id);
        Address Post(string cityName, string countryName, string streetName);
        Address Put(int id, string countryName, string cityName, string streetName);
    }
}
