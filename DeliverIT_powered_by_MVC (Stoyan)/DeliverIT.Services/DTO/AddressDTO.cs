﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTO
{
    public class AddressDTO
    {
        private readonly Address address;

        public AddressDTO(Address address)
        {
            this.address = address;
        }

        public string CountryName => this.address.Country.Name;
        public string CityName => this.address.City.Name;
        public string StreetName => this.address.StreetName;

    }
}
