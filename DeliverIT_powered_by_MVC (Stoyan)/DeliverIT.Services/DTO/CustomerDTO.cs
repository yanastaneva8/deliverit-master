﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTO
{
    public class CustomerDTO
    {
        private readonly Customer customer;

        public CustomerDTO(Customer customer)
        {
            this.customer = customer;
        }

        public string FirstName => customer.FirstName;
        public string LastName => customer.LastName;
        public string Email => customer.Email;
        public int DeliveryWarehouseId => customer.DeliveryWarehouseId;

    }
}
