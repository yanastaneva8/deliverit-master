﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTO
{
    public class CityDTO
    {
        private readonly City city;

        public CityDTO(City city)
        {
            this.city = city;
        }
        public string Name => this.city.Name;
        public string CountryName => this.city.Country.Name;

    }
}
