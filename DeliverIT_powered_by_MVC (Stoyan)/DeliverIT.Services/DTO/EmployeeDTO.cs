﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTO
{
    public class EmployeeDTO
    {
        private readonly Employee employee;

        public EmployeeDTO(Employee employee)
        {
            this.employee = employee;
        }
        

        public string FirstName => employee.FirstName;
        public string LastName => employee.LastName;
        public string Email => employee.Email;
        public int DeliveryWarehouseId => employee.WarehouseId;

    }
}
