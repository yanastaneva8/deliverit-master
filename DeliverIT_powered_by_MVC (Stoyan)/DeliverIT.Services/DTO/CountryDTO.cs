﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTO
{
    public class CountryDTO
    {
        private readonly Country country;

        public CountryDTO(Country country)
        {
            this.country = country;
        }

        public string Name => this.country.Name;
    }
}
