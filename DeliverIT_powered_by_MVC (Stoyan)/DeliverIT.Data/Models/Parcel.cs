﻿using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Parcel
    {
        /// <summary>
        /// Parcel field for weight
        /// </summary>
        private double weight;

        /// <summary>
        /// Id property, primary key of parcels table
        /// </summary>
        [Key]
        public int ParcelId { get; set; }

        /// <summary>
        /// CustomerId property, foreign key of customers table
        /// </summary>
        [Required]
        public int CustomerId { get; set; }
        
        /// <summary>
        /// Customer property
        /// </summary>
        public Customer Customer { get; set; }

        /// <summary>
        /// DepartureWarehouseId property, foreign key of warehouses table
        /// </summary>
        [Required]
        public int DepartureWarehouseId { get; set; }

        /// <summary>
        /// DepartureWarehouse property
        /// </summary>
        public Warehouse DepartureWarehouse { get; set; }

        /// <summary>
        /// ArrivalWarehouseId property, foreign key of warehouses table
        /// </summary>
        [Required]
        public int ArrivalWarehouseId { get; set; }

        /// <summary>
        /// DepartureWarehouse property
        /// </summary>
        public Warehouse ArrivalWarehouse { get; set; }

        /// <summary>
        /// Weight property, validation included
        /// </summary>
        [Required]
        public double Weight 
        {
            get
            {
                return this.weight;
            }
            set
            {
                if (value < 0)
                {
                    throw new CannotBeNegative("Weight", "double");
                }
                this.weight = value;
            }
        }
        
        /// <summary>
        /// CategoryId property, foreign key of categories table
        /// </summary>
        [Required]
        public int CategoryId { get; set; }
        
        /// <summary>
        /// Category property
        /// </summary>
        public Category Category { get; set; }


        /// <summary>
        /// Boolean property IsCollected, false by default, true if parcel collected by customer from delivery warehouse
        /// </summary>
        public bool IsCollected { get; set; } = false;

        /// <summary>
        /// ShipmentId property
        /// </summary>
        public int? ShipmentId { get; set; }

        /// <summary>
        /// Shipment property
        /// </summary>
        public Shipment Shipment { get; set; }

        /// <summary>
        /// Boolean property IsDeleted, false by defaults, true if parcel deleted by employee
        /// </summary>
        public bool IsDeleted { get; set; } = false;

    }
}
