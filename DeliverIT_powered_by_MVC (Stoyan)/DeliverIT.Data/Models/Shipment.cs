﻿using DeliverIT.Exceptions;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DeliverIT.Data.Models
{
    public class Shipment
    {
        private DateTime arrivalDate;

        /// <summary>
        /// Id property, primary key of the shipments table
        /// </summary>
        [Key]
        public int ShipmentId { get; set; }

        /// <summary>
        /// DepartureDate property
        /// </summary>
        public DateTime DepartureDate { get; set; }

        /// <summary>
        /// ArrivalDate property
        /// </summary>
        public DateTime ArrivalDate
        {
            get
            {
                return this.arrivalDate;
            }
            set
            {
                this.arrivalDate = value > this.DepartureDate
                    ? value
                    : throw new InvalidInputTime(value);
            }
        }
        /// <summary>
        /// StatusId property, foreign key of statuses table
        /// </summary>
        [Required]
        public int StatusId { get; set; }
        
        /// <summary>
        /// Status property
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// DepartureWarehouseId property, foreign key of warehouses table
        /// </summary>
        [Required]
        public int DepartureWarehouseId { get; set; }

        /// <summary>
        /// DepartureWarehouse property
        /// </summary>
        public Warehouse DepartureWarehouse { get; set; }

        /// <summary>
        /// ArrivalWarehouseId property, foreign key of warehouses table
        /// </summary>
        [Required]
        public int ArrivalWarehouseId { get; set; }

        /// <summary>
        /// DepartureWarehouse property
        /// </summary>
        public Warehouse ArrivalWarehouse { get; set; }

        /// <summary>
        /// Parcels property
        /// </summary>
        public ICollection<Parcel> Parcels { get; set; }

        /// <summary>
        /// Boolean property IsDeleted - false by default; true if shipment deleted
        /// </summary>
        public bool IsDeleted { get; set; }
    }
}
