﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Data.Models
{
    public class User
    {
        public int UserId { get; set; }
        public int RoleId { get; set; }
        public Role Role { get; set; }
        public virtual string Email { get; set; }
    }

}
