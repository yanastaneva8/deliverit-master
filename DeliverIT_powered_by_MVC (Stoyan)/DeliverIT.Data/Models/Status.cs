﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Status
    {
        /// <summary>
        /// Status fields for id and name
        /// </summary>
        private int id;
        private string name;

        /// <summary>
        /// Id property, primary key of statuses table, validation included
        /// </summary>
        [Key]
        [Required]
        public int StatusId
        {
            get
            {
                return this.id;
            }
            set
            {
                if (value < 0)
                {
                    throw new CannotBeNegative("Id", "int");
                }
                this.id = value;
            }
        }

        /// <summary>
        /// Name property, validation included
        /// </summary>
        [Required]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                int min = 3;
                int max = 20;

                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Status name", min, max);
                }
                this.name = value;
            }
        }
    }
}
