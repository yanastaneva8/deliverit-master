﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);
                context.SaveChanges();

                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);

                var warehouse = new Warehouse
                {
                    AddressId = 1,
                    IsDeleted = false,
                    WarehouseId = 1
                };

                // Assert
                Assert.AreEqual(newWarehouseService.Get(1).AddressId, warehouse.AddressId);
                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfNonexistentId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);


                // Act & Assert
                Assert.ThrowsException<NonExistentId>(() => newWarehouseService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
