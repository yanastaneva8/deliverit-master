﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void DeleteSuccessfully()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);
                context.SaveChanges();

                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);

                var testWarehouse = new Warehouse
                {
                    AddressId = 1,
                    IsDeleted = false,
                    WarehouseId = 100
                };

                context.Warehouses.Add(testWarehouse);
                context.SaveChanges();

                // Act & Assert
                Assert.IsInstanceOfType(newWarehouseService.Get(100), typeof(Warehouse));
                Assert.IsTrue(newWarehouseService.Delete(100));
                Assert.ThrowsException<NonExistentId>(() => newWarehouseService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
