﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.AddressServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public void ExecuteCorrectly_AllParametersPassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();


                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                cityServiceMock.Setup(c => c.Get("Varna")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 1));
                cityServiceMock.Setup(c => c.Get("London")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 3));
                countryServiceMock.Setup(c => c.Get("Bulgaria")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 1));
                countryServiceMock.Setup(c => c.Get("United Kingdom")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 2));

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                var test = new Address() { AddressId = 99, CityId = 1, CountryId = 1, StreetName = "Okolovrusten put 100" };
                context.Addresses.Add(test);
                context.SaveChanges();

                // Act
                var sut = newAddressService.Put(99, "United Kingdom", "London", "Fullham Suites");

                // Assert
                Assert.AreEqual(sut.AddressId, 99);
                Assert.AreEqual(sut.CityId, 3);
                Assert.AreEqual(sut.CountryId, 2);
                Assert.AreEqual(sut.StreetName, "Fullham Suites");

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowNonExistendId_NonExistentAddress()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();


                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                cityServiceMock.Setup(c => c.Get("Varna")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 1));
                cityServiceMock.Setup(c => c.Get("London")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 3));
                countryServiceMock.Setup(c => c.Get("Bulgaria")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 1));
                countryServiceMock.Setup(c => c.Get("United Kingdom")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 2));

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                // Act & Assert
                Assert.ThrowsException<NonExistentId>(() => newAddressService.Put(99, "Bulgaria", "Varna", "Hristo Botev 320"));

                context.Database.EnsureDeleted();
            }
            }

        [TestMethod]
        public void ThrowUnsynchronizedIdException_UnsynchedIdsCountryCity()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();


                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                cityServiceMock.Setup(c => c.Get("Varna")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 1));
                cityServiceMock.Setup(c => c.Get("London")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 3 && c.CountryID == 2));
                countryServiceMock.Setup(c => c.Get("Bulgaria")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 1));
                countryServiceMock.Setup(c => c.Get("United Kingdom")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 2));

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                var test = new Address() { AddressId = 99, CityId = 1, CountryId = 1, StreetName = "Okolovrusten put 100" };
                context.Addresses.Add(test);
                context.SaveChanges();

                // Act & Assert
                Assert.ThrowsException<UnsynchronizedIds>(() => newAddressService.Put(99, "Bulgaria", "London", "Street"));

                context.Database.EnsureDeleted();
            }
        }
    }
}
