﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.AddressServiceTests
{
    [TestClass]
    public class Get_Should
    {

        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                // Act
                Assert.AreEqual(newAddressService.Get().Count(), context.Addresses.Count());

            }
        }
    }
}