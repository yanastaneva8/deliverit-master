﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.AddressServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                context.SaveChanges();

                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);


                var address = new Address
                {
                    AddressId = 1,
                    CityId = 2,
                    StreetName = "Vitosha Boulevard",
                    CountryId = 1
                };


                // Assert
                Assert.AreEqual(newAddressService.Get(1).AddressId, address.AddressId);
                Assert.AreEqual(newAddressService.Get(1).CityId, address.CityId);
                Assert.AreEqual(newAddressService.Get(1).CountryId, address.CountryId);
            }
        }

        [TestMethod]
        public void ThrowIfNonexistentId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<Services.Contracts.ICountryService>();

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                Assert.ThrowsException<NonExistentId>(() => newAddressService.Get(100));
            }
        }

        [TestMethod]
        public void ThrowIfCountryNonexistent()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<Services.Contracts.ICountryService>();

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);
                var newCountryService = new CountryService(context);

                Assert.ThrowsException<NonExistentId>(() => newCountryService.Get(100));
            }
        }
    }
}
