﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.AddressServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {

            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                cityServiceMock.Setup(c => c.Get("Varna")).Returns(context.Cities.FirstOrDefault(c => c.CityId == 1));
                countryServiceMock.Setup(c => c.Get("Bulgaria")).Returns(context.Countries.FirstOrDefault(c => c.CountryId == 1));

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                // Act
                var sut = newAddressService.Post("Varna", "Bulgaria", "Morskata 420");

                // Assert
                Assert.IsInstanceOfType(sut, typeof(Address));
                Assert.AreEqual(sut.CityId, 1);
                Assert.AreEqual(sut.CountryId, 1);
                Assert.AreEqual(sut.StreetName, "Morskata 420");

                context.Database.EnsureDeleted();
            }
        }
    }
}
