﻿using System.Linq;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.CustomerServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseService = new Mock<IWarehouseService>();

                var newCustomerService = new CustomerService(context, warehouseService.Object);

                var customer = new Customer
                {
                    FirstName = "Gosho",
                    LastName = "Customera",
                    Email = "goshocustomera@deliverit.com",
                    CustomerId = 100,
                    IsDeleted = false,
                    DeliveryWarehouseId = 4
                };

                context.Customers.Add(customer);
                context.SaveChanges();

                // Assert
                Assert.AreEqual(newCustomerService.Get("Gosho", "Customera", "goshocustomera@deliverit.com").Count(), 1);

                context.Database.EnsureDeleted();

            }
        }
    }
}
