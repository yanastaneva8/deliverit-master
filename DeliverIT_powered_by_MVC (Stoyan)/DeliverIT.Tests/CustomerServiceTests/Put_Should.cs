﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.CustomerServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseService = new Mock<IWarehouseService>();
                warehouseService.Setup(x => x.Get(1)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 1));
                warehouseService.Setup(x => x.Get(2)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 2));

                var newCustomerService = new CustomerService(context, warehouseService.Object);

                var customer = new Customer
                {
                    FirstName = "Gosho",
                    LastName = "Customera",
                    Email = "goshocustomera@deliverit.com",
                    CustomerId = 100,
                    IsDeleted = false,
                    DeliveryWarehouseId = 4
                };

                context.Customers.Add(customer);
                context.SaveChanges();

                // Act
                var sut = newCustomerService.Put(100, "Pepi", "Kupuva4a", "pepi@gmail.com", 2);

                // Assert
                Assert.AreEqual(sut.CustomerId, 100);
                Assert.AreEqual(sut.Email, "pepi@gmail.com");
                Assert.AreEqual(sut.FirstName, "Pepi");
                Assert.AreEqual(sut.LastName, "Kupuva4a");

                context.Database.EnsureDeleted();
            }
        }
    }
}
