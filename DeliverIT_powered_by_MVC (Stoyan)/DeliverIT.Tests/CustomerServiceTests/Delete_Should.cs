﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.CustomerServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseService = new Mock<IWarehouseService>();
                warehouseService.Setup(x => x.Get(1)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 1));

                var newCustomerService = new CustomerService(context, warehouseService.Object);

                var customer = new Customer
                {
                    FirstName = "Gosho",
                    LastName = "Customera",
                    Email = "goshocustomera@deliverit.com",
                    CustomerId = 100,
                    IsDeleted = false,
                    DeliveryWarehouseId = 4
                };

                context.Customers.Add(customer);
                context.SaveChanges();

                // Assert
                Assert.IsInstanceOfType(newCustomerService.Get(100), typeof(Customer));
                Assert.IsTrue(newCustomerService.Delete(100));
                Assert.ThrowsException<NonExistentId>(() => newCustomerService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
