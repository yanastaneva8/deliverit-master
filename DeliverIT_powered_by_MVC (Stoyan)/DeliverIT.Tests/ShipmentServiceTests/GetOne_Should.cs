﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class GetOne_Should
    {
        [TestMethod]
        public void FindAndReturnProperObjectById()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var sut1 = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 1,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                });
                context.SaveChanges();

                //Act
                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);
                var sut2 = testShipmentService.Get(99);

                //Assert
                Assert.IsTrue(sut1.Entity == sut2);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfInvalidIdIsPassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(()=>testShipmentService.Get(99));

                context.Database.EnsureDeleted();
            }
        }
    }
}
