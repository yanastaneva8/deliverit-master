﻿using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Data;
using DeliverIT.Data.Models;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class AddParcelsToShipment_Should
    {
        [DataRow(99, 2, 1, 2, 99)]
        [TestMethod]
        public void AddProperlyParcelsToShipment(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,

                });

                var parcelsToAdd = new List<Parcel>
                {
                    new Parcel
                    {
                        ParcelId = parcelId,
                        CustomerId = 1,
                        ArrivalWarehouseId = arrivalWarehouseId,
                        DepartureWarehouseId = departureWarehouseId,
                        Weight = 2.5,
                        CategoryId = 1,
                    },
                    new Parcel
                    {
                        ParcelId = parcelId+1,
                        CustomerId = 1,
                        ArrivalWarehouseId = arrivalWarehouseId,
                        DepartureWarehouseId = departureWarehouseId,
                        Weight = 2.5,
                        CategoryId = 1,
                    }
                };
                context.Parcels.AddRange(parcelsToAdd);

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                testShipmentService.AddParcelsToShipment(shipmentId, new int[] { parcelId, parcelId + 1 });
                var sut = context.Shipments.FirstOrDefault(sh => sh.ShipmentId == shipmentId);


                //Assert
                Assert.IsTrue(sut.Parcels.Count == 2);
                Assert.IsTrue(sut.Parcels.Except(parcelsToAdd).Count()==0);

            }
        }


        [DataRow(99, 2, 1, 2, 99)]
        [TestMethod]
        public void ThrowIfInvalidParcelIdsArePassed(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(() => testShipmentService.AddParcelsToShipment(99, new int[] { parcelId, parcelId + 1 }));

            }
        }




    }
}
