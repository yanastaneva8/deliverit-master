﻿using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Data;
using DeliverIT.Data.Models;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class AdvanceStatus_Should
    {
        [DataRow(1)]
        [DataRow(2)]
        [TestMethod]
        public void ProperlyAdvanceTheStatusOfAShipmentById(int statusId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var sut = shipmentEntry.Entity;
                testShipmentService.AdvanceStatus(99);
                //Assert
                Assert.AreEqual(sut.StatusId, statusId+1);

                context.Database.EnsureDeleted();
            }

        }


        [TestMethod]
        public void ThrowIfNoSuchShipmentExists()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(() => testShipmentService.AdvanceStatus(99));

            }

        }

        [TestMethod]
        public void ThrowIfStatusMaxed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 3,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<StatusReached>(() => testShipmentService.AdvanceStatus(99));

                context.Database.EnsureDeleted();
            }

        }
    }
}
