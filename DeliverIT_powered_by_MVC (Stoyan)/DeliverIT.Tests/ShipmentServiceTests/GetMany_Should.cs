﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class GetMany_Should
    {
        [TestMethod]
        public void GetAllShipmentsProperlyIfNoParamIsPassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var databaseShipments = context.Shipments;
                var sut = testShipmentService.GetMany(null);

                //Assert
                Assert.IsTrue(sut.Count() == databaseShipments.Count());
                Assert.AreEqual(sut.Select(sh=>sh.ShipmentId) //giving trouble because of .include() without .Select)_
                    .Except(context.Shipments.Select(sh=>sh.ShipmentId))
                    .Count(), 0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        [DataRow(1)]
        [DataRow(2)]
        public void GetAllShipmentsOfCustomerOnlyIfParamIsPassed(int customerId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var sut = testShipmentService.GetMany(customerId);
                var databaseShipmentsOfCustomer = context.Shipments.Where(sh => sh.Parcels.Any(pa => pa.CustomerId == customerId));

                Assert.IsTrue(sut.Count() != 0);
                Assert.IsTrue(databaseShipmentsOfCustomer.Count() == sut.Count());
                Assert.IsTrue(
                    sut.Select(sh=>sh.ShipmentId)
                    .Except(databaseShipmentsOfCustomer.Select(sh=>sh.ShipmentId))
                    .Count() == 0);
                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnEmptyListIfNoCustomerMatch()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var sut = testShipmentService.GetMany(99);
                
                //Assert
                Assert.IsTrue(sut.Count() == 0);

                context.Database.EnsureDeleted();
            }

        }
    }
}
