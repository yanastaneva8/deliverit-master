﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void CreateProperObjectIfProvidedWithValidParams()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                warehouseServiceMock.Setup(whsvc => whsvc.Get(3)).Returns(context.Warehouses.FirstOrDefault(ware => ware.AddressId == 3));
                warehouseServiceMock.Setup(whsvc => whsvc.Get(4)).Returns(context.Warehouses.FirstOrDefault(ware => ware.AddressId == 4));

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);
                
                //Act
                var sut = testShipmentService.Post("12/12/2021", "15/12/2021", 3, 4);
                var comparer = context.Shipments.ToList().TakeLast(1).FirstOrDefault();

                //Assert
                Assert.AreSame(sut, comparer);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfDatesAreInconsistent()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                warehouseServiceMock.Setup(whsvc => whsvc.Get(3)).Returns(context.Warehouses.FirstOrDefault(ware => ware.AddressId == 3));
                warehouseServiceMock.Setup(whsvc => whsvc.Get(4)).Returns(context.Warehouses.FirstOrDefault(ware => ware.AddressId == 4));

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);


                //Act & Assert
                Assert.ThrowsException<InvalidInputTime>(() => testShipmentService.Post("12/12/2021", "10/12/2021", 3, 4));

                context.Database.EnsureDeleted();
            }
        }

    }
}
