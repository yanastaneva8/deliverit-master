﻿using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Data;
using DeliverIT.Data.Models;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class ReceiveShipment_Should
    {
        [DataRow(99, 2, 1, 2, 99)]
        [TestMethod]
        public void ProperlyUpdateStatusInDatabase(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,
                    Parcels = new List<Parcel>()
                    {
                        new Parcel
                        {
                            ParcelId = parcelId,
                            CustomerId = 1,
                            ArrivalWarehouseId = arrivalWarehouseId,
                            DepartureWarehouseId = departureWarehouseId,
                            Weight = 2.5,
                            CategoryId = 1,
                            ShipmentId = shipmentId
                        }
                    }

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                testShipmentService.ReceiveShipment(shipmentId);
                var sut = context.Shipments.First(sh => sh.ShipmentId == shipmentId);

                //Assert
                Assert.AreEqual(sut.StatusId, statusId + 1);
            }
        }

        [DataRow(99, 1, 1, 2, 99)]
        [TestMethod]
        public void ThrowIfShipmentAtMaxStatus(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,
                    Parcels = new List<Parcel>()
                    {
                        new Parcel
                        {
                            ParcelId = parcelId,
                            CustomerId = 1,
                            ArrivalWarehouseId = arrivalWarehouseId,
                            DepartureWarehouseId = departureWarehouseId,
                            Weight = 2.5,
                            CategoryId = 1,
                            ShipmentId = shipmentId
                        }
                    }
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<CannotUpdateStatus>(() => testShipmentService.ReceiveShipment(shipmentId));
            }
        }
    }
}
