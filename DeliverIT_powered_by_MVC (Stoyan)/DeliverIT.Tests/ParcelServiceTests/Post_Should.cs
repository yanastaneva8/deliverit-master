﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void CreateProperInstanceOfType()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                var addressServiceMock = new Mock<IAddressService>();


                customerServiceMock.Setup(x => x.Get(1)).Returns(context.Customers.FirstOrDefault(c => c.CustomerId == 1));
                warehouseServiceMock.Setup(x => x.Get(1)).Returns(context.Warehouses.FirstOrDefault(c => c.WarehouseId == 1));
                warehouseServiceMock.Setup(x => x.Get(2)).Returns(context.Warehouses.FirstOrDefault(c => c.WarehouseId == 2));


                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act
                var sut = newParcelService.Post(1, 2, 1, 2.5, 2, 1);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(Parcel));
                Assert.AreEqual(sut.CustomerId, 1);
                Assert.AreEqual(sut.DepartureWarehouseId, 2);
                Assert.AreEqual(sut.ArrivalWarehouseId, 1);
                Assert.AreEqual(sut.Weight, 2.5);
                Assert.AreEqual(sut.CategoryId, 2);
                Assert.AreEqual(sut.ShipmentId, 1);

                context.Database.EnsureDeleted();
            }
        }
    }
}
