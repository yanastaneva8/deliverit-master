﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class GetStatusOfShipmentHoldingParcel_Should
    {
        [TestMethod]
        public void GetProperStatusOfShipmentIfAllParamsValid()
        {
             //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    StatusId = 2,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    ArrivalWarehouseId = 1,
                    DepartureDate = DateTime.Now,
                    DepartureWarehouseId = 2,
                    IsDeleted = false
                }) ;

                context.Parcels.Add(new Parcel
                {
                    ParcelId=99,
                    ShipmentId = 99,
                    ArrivalWarehouseId=1,
                    DepartureWarehouseId=2,
                    CategoryId=1,
                    CustomerId=1,
                    IsCollected=false,
                    Weight=2.5,
                    IsDeleted=false,
                });
               
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act
                var sut = newParcelService.GetStatusOfShipmentHoldingParcel(99, 1);

                //Assert
                Assert.IsInstanceOfType(sut, typeof(Status));
                Assert.IsTrue(sut.StatusId == 2);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfNoSuchParcelIsFound()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();


                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act & Assert

                Assert.ThrowsException<NonExistentId>(() => newParcelService.GetStatusOfShipmentHoldingParcel(99, 1));

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfParcelDoesntBelongToCustomer()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();


                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act & Assert

                Assert.ThrowsException<ParcelDoesNotBelongToCustomer>(() => newParcelService.GetStatusOfShipmentHoldingParcel(1, 99));

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfParcelNotAssignedToShipment()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                context.Parcels.Add(new Parcel
                {
                    ParcelId = 99,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    CategoryId = 1,
                    CustomerId = 1,
                    IsCollected = false,
                    Weight = 2.5,
                    IsDeleted = false,
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();


                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act & Assert

                Assert.ThrowsException<ParcelNotAssignedToShipment>(() => newParcelService.GetStatusOfShipmentHoldingParcel(99, 1));

                context.Database.EnsureDeleted();
            }
        }
    }
}
