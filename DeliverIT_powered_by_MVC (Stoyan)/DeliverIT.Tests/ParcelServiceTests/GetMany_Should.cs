﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class GetMany_Should
    {
        [TestMethod]
        public void GetProperItemBasedOnWeight()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Assert
                var sut = newParcelService.Get(2.5, 0, 0, 0, 0, "w", "a").ToList();

                Assert.IsTrue(sut.Count != 0);
                Assert.AreEqual(sut.Except(context.Parcels.Where(p => p.Weight == 2.5).OrderBy(p => p.Weight).ToList()).Count(),0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void GetProperItemBasedOnCustomerId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Assert
                var sut = newParcelService.Get(0, 1, 0, 0, 0, "w", "a").ToList();

                Assert.IsTrue(sut.Count != 0);
                Assert.AreEqual(sut.Except(context.Parcels.Where(p => p.CustomerId == 1).OrderBy(p => p.Weight).ToList()).Count(), 0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void GetProperItemBasedOnArrivalWarehouseId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Assert
                var sut = newParcelService.Get(0, 0, 1, 0, 0, "w", "a").ToList();

                Assert.IsTrue(sut.Count != 0);
                Assert.AreEqual(sut.Except(context.Parcels.Where(p => p.ArrivalWarehouseId == 1).OrderBy(p => p.Weight).ToList()).Count(), 0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void GetProperItemBasedOnDepartureWarehouseId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Assert
                var sut = newParcelService.Get(0, 0, 0, 1, 0, "w", "a").ToList();

                Assert.IsTrue(sut.Count != 0);
                Assert.AreEqual(sut.Except(context.Parcels.Where(p => p.DepartureWarehouseId == 1).OrderBy(p => p.Weight).ToList()).Count(), 0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void GetProperItemBasedOnCategoryId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Assert
                var sut = newParcelService.Get(0, 0, 0, 0, 1, "w", "a").ToList();

                Assert.IsTrue(sut.Count != 0);
                Assert.AreEqual(sut.Except(context.Parcels.Where(p => p.CategoryId == 1).OrderBy(p => p.Weight).ToList()).Count(), 0);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void OrderProperlyByWeightAscDescDefault()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                var weightAscTester = newParcelService.Get(0, 0, 0, 0, 0, "w", "a").ToList();
                var weightDescTester = newParcelService.Get(0, 0, 0, 0, 0, "w", "d").ToList();
                var weightDefTester = newParcelService.Get(0, 0, 0, 0, 0, "w", "").ToList();
                var shipmentArrivalDateAscTester = newParcelService.Get(0, 0, 0, 0, 0, "ad", "a").ToList();
                var shipmentArrivalDateDescTester = newParcelService.Get(0, 0, 0, 0, 0, "ad", "d").ToList();
                var shipmentArrivalDateDefTester = newParcelService.Get(0, 0, 0, 0, 0, "ad", "").ToList();
                var weightArrivalDateAscTester = newParcelService.Get(0, 0, 0, 0, 0, "wad", "a").ToList();
                var weightArrivalDateDescTester = newParcelService.Get(0, 0, 0, 0, 0, "wad", "d").ToList();
                var weightArrivalDateDefTester = newParcelService.Get(0, 0, 0, 0, 0, "wad", "").ToList();

                //Assert
                Assert.IsTrue(weightAscTester.Count != 0);
                Assert.IsTrue(weightDescTester.Count != 0);
                Assert.IsTrue(weightDefTester.Count != 0);

                Assert.IsTrue(shipmentArrivalDateAscTester.Count != 0);
                Assert.IsTrue(shipmentArrivalDateDescTester.Count != 0);
                Assert.IsTrue(shipmentArrivalDateDefTester.Count != 0);

                Assert.IsTrue(weightArrivalDateAscTester.Count != 0);
                Assert.IsTrue(weightArrivalDateDescTester.Count != 0);
                Assert.IsTrue(weightArrivalDateDefTester.Count != 0);

                Assert.AreEqual(weightAscTester.Except(context.Parcels.OrderBy(p => p.Weight).ToList()).Count(), 0);
                Assert.AreEqual(weightAscTester.Except(context.Parcels.OrderByDescending(p => p.Weight).ToList()).Count(), 0);
                Assert.AreEqual(weightAscTester.Except(context.Parcels.OrderBy(p => p.Weight).ToList()).Count(), 0);

                Assert.AreEqual(shipmentArrivalDateAscTester.Except(context.Parcels.OrderBy(p => p.Shipment.ArrivalDate)).Count(), 0);
                Assert.AreEqual(shipmentArrivalDateAscTester.Except(context.Parcels.OrderByDescending(p => p.Shipment.ArrivalDate)).Count(), 0);
                Assert.AreEqual(shipmentArrivalDateAscTester.Except(context.Parcels.OrderBy(p => p.Shipment.ArrivalDate)).Count(), 0);

                Assert.AreEqual(weightArrivalDateAscTester.Except(context.Parcels.OrderBy(p => p.Weight).ThenBy(p => p.Shipment.ArrivalDate)).Count(), 0);
                Assert.AreEqual(weightArrivalDateAscTester.Except(context.Parcels.OrderByDescending(p => p.Weight).ThenByDescending(p => p.Shipment.ArrivalDate)).Count(), 0);
                Assert.AreEqual(weightArrivalDateAscTester.Except(context.Parcels.OrderBy(p => p.Weight).ThenBy(p => p.Shipment.ArrivalDate)).Count(), 0);

                context.Database.EnsureDeleted();
            }
        }



    }
}
