﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public void EditCorrectlyEachPropertyOfObject()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                var addressServiceMock = new Mock<IAddressService>();


                customerServiceMock.Setup(cu => cu.Get(1)).Returns(context.Customers.FirstOrDefault(c => c.CustomerId == 1));
                warehouseServiceMock.Setup(wh => wh.Get(1)).Returns(context.Warehouses.FirstOrDefault(c => c.WarehouseId == 1));
                warehouseServiceMock.Setup(wh => wh.Get(2)).Returns(context.Warehouses.FirstOrDefault(c => c.WarehouseId == 2));
                shipmentServiceMock.Setup(sh => sh.Get(1)).Returns(context.Shipments.FirstOrDefault(sh => sh.ShipmentId == 1));
                shipmentServiceMock.Setup(sh => sh.Get(2)).Returns(context.Shipments.FirstOrDefault(sh => sh.ShipmentId == 2));

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                var testParcel = new Parcel() { ParcelId = 99, CustomerId=1,CategoryId=2, ArrivalWarehouseId = 1, DepartureWarehouseId = 2, ShipmentId = 1 };
                context.Parcels.Add(testParcel);

                context.SaveChanges();

                //Act
                var sut = newParcelService.Put(99, 2, 1, 2);
                

                //Assert
                Assert.AreEqual(sut.ParcelId, 99);
                Assert.AreEqual(sut.ArrivalWarehouseId, 2);
                Assert.AreEqual(sut.DepartureWarehouseId, 1);
                Assert.AreEqual(sut.CategoryId, 2);
                Assert.AreEqual(sut.ShipmentId, 2);

                context.Database.EnsureDeleted();
            }

        }
    }
}
