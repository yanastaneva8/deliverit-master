﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void DeleteAndReturnTrueIfValidParcelIsPassedById()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                var addressServiceMock = new Mock<IAddressService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                var testParcel = new Parcel() 
                { 
                    ParcelId = 99, 
                    CustomerId = 1, 
                    CategoryId = 2, 
                    ArrivalWarehouseId = 1, 
                    DepartureWarehouseId = 2, 
                    ShipmentId = 1 
                };
                context.Parcels.Add(testParcel);

                context.SaveChanges();

                //Act & Assert
                Assert.IsInstanceOfType(newParcelService.Get(99), typeof(Parcel));
                Assert.IsTrue(newParcelService.Delete(99));
                Assert.ThrowsException<NonExistentId>(() => newParcelService.Get(99));

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ReturnFalseIfInvalidParcelIsPassedById()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                var addressServiceMock = new Mock<IAddressService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                context.SaveChanges();

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(() => newParcelService.Get(99));
                Assert.IsFalse(newParcelService.Delete(99));

                context.Database.EnsureDeleted();
            }
        }




    }
}
