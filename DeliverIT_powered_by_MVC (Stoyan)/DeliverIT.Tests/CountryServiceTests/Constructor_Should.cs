﻿using DeliverIT.Data;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.CountryServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            var context = new DeliverITContext(options);

            var newCountryService = new CountryService(context);

            //Act & Assert
            Assert.IsInstanceOfType(newCountryService, typeof(CountryService));
        }

    } 
}
