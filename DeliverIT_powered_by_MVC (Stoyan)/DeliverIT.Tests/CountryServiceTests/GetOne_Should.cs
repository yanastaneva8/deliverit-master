﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;


namespace DeliverIT.Tests.CountryServiceTests
{
    [TestClass]
    public class GetOne_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var newCountryService = new CountryService(context);

                var country = new Country()
                {
                    CountryId = 300,
                    Name = "2loopland"
                };

                // Act
                context.Countries.Add(country);
                context.SaveChanges();

                // Assert
                Assert.IsTrue(newCountryService.Get(300).CountryId == country.CountryId);
                Assert.IsTrue(newCountryService.Get(300) == country);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsNonExistentId_Country()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var newCountryService = new CountryService(context);

                // Act & Assert
                Assert.ThrowsException<NonExistentId>(() => newCountryService.Get(99));
            }
        }

        [TestMethod]
        public void ThrowsNonExistentName_Country()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var newCountryService = new CountryService(context);

                // Act & Assert
                Assert.ThrowsException<NonExistentName>(() => newCountryService.Get("non existent country"));
            }
        }
    }
}
