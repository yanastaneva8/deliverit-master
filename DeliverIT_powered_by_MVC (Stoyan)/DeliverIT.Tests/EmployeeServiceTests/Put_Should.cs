﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseService = new Mock<IWarehouseService>();
                warehouseService.Setup(x => x.Get(1)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 1));
                warehouseService.Setup(x => x.Get(2)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 2));

                var newEmployeeService = new EmployeeService(context, warehouseService.Object);

                var testEmployee = new Employee()
                {
                    FirstName = "Pesho",
                    LastName = "Admina",
                    Email = "peshkata@deliverit.com",
                    EmployeeId = 100,
                    IsDeleted = false,
                    WarehouseId = 1
                };
                context.Employees.Add(testEmployee);
                context.SaveChanges();

                // Act
                var sut = newEmployeeService.Put(100, "Pepi", "Admin4eto", "pepi@deliverit.com", 2);

                // Assert
                Assert.AreEqual(sut.EmployeeId, 100);
                Assert.AreEqual(sut.Email, "pepi@deliverit.com");
                Assert.AreEqual(sut.FirstName, "Pepi");
                Assert.AreEqual(sut.LastName, "Admin4eto");

                context.Database.EnsureDeleted();
            }
        }
    }
}
