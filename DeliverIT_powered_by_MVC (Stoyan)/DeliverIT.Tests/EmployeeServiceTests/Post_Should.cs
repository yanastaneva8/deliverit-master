﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var warehouseService = new Mock<IWarehouseService>();
                warehouseService.Setup(x => x.Get(1)).Returns(context.Warehouses.FirstOrDefault(w => w.WarehouseId == 1));

                var newEmployeeService = new EmployeeService(context, warehouseService.Object);

                // Act
                var sut = newEmployeeService.Post("Pesho", "Admina", "peshkata@deliverit.com", 1);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(Employee));

                context.Database.EnsureDeleted();
            }
        }
    }
}
