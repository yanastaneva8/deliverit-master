﻿using DeliverIT.Exceptions;
using DeliverIT.MVC_Session.API.Helpers;
using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace DeliverIT.MVC_Session.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeesController : ControllerBase
    {
        /// <summary>
        /// Employee service and auth helper fields
        /// </summary>
        private readonly IEmployeeService employeeService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with employee service and auth helper injections
        /// </summary>
        /// <param name="employeeService">Employee service</param>
        /// <param name="authHelper">Authentication helper</param>
        public EmployeesController(IEmployeeService employeeService,
            IAuthHelper authHelper)
        {
            this.employeeService = employeeService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get employee by id
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                    return this.Ok(employeeService.Get(id));
                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get employees by search criteria
        /// </summary>
        /// <param name="empEmail">Employee email for authentication</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <returns></returns>
        [HttpGet("")]
        public IActionResult Get([FromHeader] string empEmail,
            [FromQuery] string firstName = null,
            string lastName = null,
            string email = null)
        {
            if (this.authHelper.GetValidEmployee(empEmail) != null)
            {
                var employees = employeeService.Get(firstName, lastName, email);

                if (employees.Count() == 0)
                    return this.NotFound(ConstantStringMessages.employeeNotFound);

                return this.Ok(employees);
            }
            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Create new employee
        /// </summary>
        /// <param name="empEmail">Employee email for authentication</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post([FromHeader] string empEmail,
            [FromQuery] string firstName,
            string lastName,
            string email,
            int warehouseId)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(empEmail) != null)
                    return this.Ok(employeeService.Post(firstName, lastName, email, warehouseId));
                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Delete employee 
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string email,
            int id)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                var result = employeeService.Delete(id);

                if (result)
                    return this.Ok(ConstantStringMessages.employeeDeleted);

                return this.NotFound(ConstantStringMessages.employeeNotFound);
            }

            return Unauthorized(ConstantStringMessages.unauthorized);

        }

        /// <summary>
        /// Update employee details
        /// </summary>
        /// <param name="empEmail">Employee email for authentication</param>
        /// <param name="id">Employee Id</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string empEmail,
            int id,
            [FromQuery] string firstName = null,
            string lastName = null,
            string email = null,
            int warehouseId = 0)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(empEmail) != null)
                    return this.Ok(employeeService.Put(id, firstName, lastName, email, warehouseId));

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return this.NotFound(e.Message);
            }
        }

        /// <summary>
        /// Search employee by keyword
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="seeker">Seeker keyword</param>
        /// <returns></returns>
        [HttpGet("searchallprops")]
        public IActionResult Search([FromHeader] string email,
            [FromQuery] string seeker)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                if (seeker == null)
                    return this.BadRequest(ConstantStringMessages.criteriaNotSpecified);

                var employees = employeeService.Search(seeker);

                if (employees.Count() == 0)
                    return this.NotFound(ConstantStringMessages.employeeNotFound);

                return this.Ok(employees);

            }

            return Unauthorized(ConstantStringMessages.unauthorized);

        }
    }
}
