﻿using Microsoft.AspNetCore.Mvc;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.MVC_Session.API.Helpers;

namespace DeliverIT.MVC_Session.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class ParcelsController : ControllerBase
    {
        /// <summary>
        /// Parcel service and auth helper fields
        /// </summary>
        IParcelService parcelService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with parcel service and auth helper injection
        /// </summary>
        /// <param name="parcelService">Parcel service</param>
        /// <param name="authHelper">Authentication helper</param>
        public ParcelsController(IParcelService parcelService,
            IAuthHelper authHelper)
        {
            this.parcelService = parcelService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get parcels by search criteria
        /// </summary>
        /// <param name="empEmail">Employee email for authentication</param>
        /// <param name="weight">Weight</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="arrivalWarehouseId">Arrival warehouse Id</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="categoryId">Category Id</param>
        /// <param name="sortingCriteria">Sorting criteria - w (weight), ad (arrival date), wad (both)</param>
        /// <param name="sortingOrder">Sorting order - a (ascending), d (descending)</param>
        /// <returns>Parcels</returns>
        [HttpGet()]
        public IActionResult Get([FromHeader] string empEmail,
            [FromQuery] double weight,
            int customerId,
            int arrivalWarehouseId,
            int departureWarehouseId,
            int categoryId,
            string sortingCriteria,
            string sortingOrder)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(empEmail) != null)
                {
                    return Ok(parcelService.Get(weight,
                                                customerId,
                                                arrivalWarehouseId,
                                                departureWarehouseId,
                                                categoryId,
                                                sortingCriteria,
                                                sortingOrder));
                }

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get parcel by id
        /// </summary>
        /// <param name="id">Parcel Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns>Parcel</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id,
            [FromHeader] string email)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                    return Ok(parcelService.Get(id));

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Gets the status of the shipment containing the parcel with selected id
        /// </summary>
        /// <param name="email"></param>
        /// <param name="customerId"></param>
        /// <param name="id"></param>
        /// <returns>Status</returns>
        [HttpGet("{id}/status")]
        public IActionResult GetStatus([FromHeader] string email,int id, int customerId)
        {
            if(this.authHelper.GetValidEmployee(email) != null 
                || this.authHelper.DoesEmailBelongToCustomerId(email, customerId))
            {
                try
                {
                    return Ok(parcelService.GetStatusOfShipmentHoldingParcel(id,customerId));
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
                catch(ParcelDoesNotBelongToCustomer e)
                {
                    return NotFound(e.Message);
                }
                catch(ParcelNotAssignedToShipment e)
                {
                    return NotFound(e.Message);
                }
            }
            return Unauthorized(ConstantStringMessages.unauthorized);

        }

        /// <summary>
        /// Create new parcel
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="customerId">Customer Id</param>
        /// <param name="shipmentId">Shipment Id</param>
        /// <param name="arrivalWarehouseId">Arrival warehouse Id</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="weight">Weight</param>
        /// <param name="categoryId">Category Id</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromHeader] string email,
            [FromQuery] int customerId,
            int arrivalWarehouseId,
            int departureWarehouseId,
            double weight,
            int categoryId,
            int? shipmentId)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    var parcel = parcelService.Post(customerId,
                        arrivalWarehouseId,
                        departureWarehouseId,
                        weight,
                        categoryId,
                        shipmentId);

                    return Created("post", parcel);
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
                catch (NonExistentName e)
                {
                    return NotFound(e.Message);
                }
                catch (AlreadyExisting e)
                {
                    return BadRequest(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Update parcel properties
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Parcel Id</param>
        /// <param name="newArrivalWarehouseId">New arrival warehouse Id</param>
        /// <param name="newDepartureWarehouseId">New departure warehouse Id</param>
        /// <param name="shipmentId">Shipment Id</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string email,
            int id, [FromQuery]
            int newArrivalWarehouseId,
            int newDepartureWarehouseId,
            int shipmentId)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    return Ok(parcelService.Put(id, newArrivalWarehouseId, newDepartureWarehouseId, shipmentId));
                }

                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Delete parcel
        /// </summary>
        /// <param name="email">Emal for authentication</param>
        /// <param name="id">Parcel Id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string email,
            int id)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                if (parcelService.Delete(id))
                    return Ok(ConstantStringMessages.parcelDeleted);

                return NotFound(ConstantStringMessages.parcelNotFound);
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }
    }
}
