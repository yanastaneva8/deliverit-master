﻿using DeliverIT.Exceptions;
using DeliverIT.MVC_Session.API.Helpers;
using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;

namespace DeliverIT.MVC_Session.Controllers.API
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : ControllerBase
    {
        /// <summary>
        /// Address service and auth helper fields
        /// </summary>
        private readonly IAddressService addressService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with address service and auth helper injection
        /// </summary>
        /// <param name="addressService">Address service</param>
        /// <param name="authHelper">Authentication helper</param>
        public AddressesController(IAddressService addressService, 
            IAuthHelper authHelper)
        {
            this.addressService = addressService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all addresses
        /// </summary>
        ///<param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult Get([FromHeader] string email)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                    return Ok(addressService.Get());
                return Unauthorized();
            }
            catch (NonExistentName)
            {
                return NotFound(ConstantStringMessages.addressNotFound);
            }
            
        }

        /// <summary>
        /// Get address by id
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                    return Ok(addressService.Get(id));
                return Unauthorized();
            }
            catch (NonExistentId)
            {
                return NotFound(ConstantStringMessages.addressNotFound);
            }
        }

        /// <summary>
        /// Create address
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <returns></returns>
        [HttpPost()]
        public IActionResult Post([FromHeader] string email,
            string cityName,
            string countryName,
            string streetName)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    return Created("post", addressService.Post(cityName, countryName, streetName));
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
                catch (NonExistentName e)
                {
                    return NotFound(e.Message);
                }
            }
            return Unauthorized();
        }

        /// <summary>
        /// Change properties of address with given Id
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <param name="countryName">Country name</param>
        /// <param name="cityName">City name</param>
        /// <param name="streetName">Street name</param>
        /// <param name="email">Email for authentication</param>
        /// <returns>The updated address</returns>
        [HttpPut()]
        public IActionResult Put([FromHeader] string email, 
            int id, 
            string countryName=null, 
            string cityName=null, 
            string streetName=null)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                    return Ok(addressService.Put(id, countryName, cityName, streetName));
                return Unauthorized();
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
            catch (NonExistentName e)
            {
                return NotFound(e.Message);
            }
            catch (AlreadyExisting e)
            {
                return NotFound(e.Message);
            }
            catch (UnsynchronizedIds e)
            {
                return NotFound(e.Message);
            }
            

        }
    }
}
