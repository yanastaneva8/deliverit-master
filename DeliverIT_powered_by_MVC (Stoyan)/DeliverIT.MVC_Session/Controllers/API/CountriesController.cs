﻿using Microsoft.AspNetCore.Mvc;
using DeliverIT.Services.Contracts;
using DeliverIT.Exceptions;
using DeliverIT.MVC_Session.API.Helpers;

namespace DeliverIT.MVC_Session.Controllers.API
{
    [ApiController]
    [Route("api/[controller]")]
    public class CountriesController : ControllerBase
    {
        /// <summary>
        /// Country service and auth helper fields
        /// </summary>
        private readonly ICountryService countryService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with country service and auth helper injection
        /// </summary>
        /// <param name="countryService">Country service</param>
        /// <param name="authHelper">Authentication helper</param>
        public CountriesController(ICountryService countryService, 
            IAuthHelper authHelper)
        {
            this.countryService = countryService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all countries
        /// </summary>
        /// <returns>All countries</returns>
        [HttpGet("")]
        public IActionResult Get([FromHeader] string email)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                var countries = this.countryService.GetAll();
                return Ok(countries);
            }
            return Unauthorized();

        }

        /// <summary>
        /// Get country by id
        /// </summary>
        /// <param name="id">Country Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns>Country with input Id</returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                {
                    var country = this.countryService.Get(id);
                    return Ok(country);
                }
                return Unauthorized();
            }
            catch (NonExistentName e)
            {
                return NotFound(e.Message);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

    }
}
