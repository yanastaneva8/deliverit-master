﻿using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using System;
using System.Linq;

namespace DeliverIT.MVC_Session.API.Helpers
{
    public class AuthHelper : IAuthHelper
    {
        private readonly IEmployeeService employeeService;
        private readonly ICustomerService customerService;

        public AuthHelper(IEmployeeService userService, ICustomerService customerService)
        {
            this.employeeService = userService;
            this.customerService = customerService;
        }

        public Employee GetValidEmployee(string email)
        {
            try
            {
                return this.employeeService.GetByEmail(email);
            }
            catch (NonExistentName)
            {
                return null;
            }

        }

        public Customer GetValidCustomer(string email)
        {
            try
            {
                return this.customerService.GetByEmail(email);
            }
            catch (NonExistentName)
            {
                return null;
            }
        }

        public bool DoesEmailBelongToCustomerId(string email, int customerId)
        {
            try
            {
                var customer = this.customerService.Get(customerId);
                return customer.Email == email;
            }
            catch (NonExistentId)
            {
                return false;
            }
        }

        public bool DoesEmailBelongToEmployeeId(string email, int employeeId)
        {
            try
            {
                var employee = this.employeeService.Get(employeeId);
                return employee.Email == email;
            }
            catch (NonExistentId)
            {
                return false;
            }


        }






    }
}
