﻿using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DeliverIT.MVC_Session.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUserService userService;

        public AccountController(IUserService userService)
        {
            this.userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login (string email)
        {
            //TODO: Authenticate against database
            if (string.IsNullOrEmpty(email))
                return View("Shared/Error");

            var user = this.userService.GetByEmail(email);
            if (user == null)
                return Unauthorized();

            //TODO: Load email & role into session
            this.HttpContext.Session.SetString("email", user.Email);
            this.HttpContext.Session.SetString("role", user.Role.Name);

            //return RedirectToAction("Profile");
            return View("Profile");
        }

        [HttpGet]
        public IActionResult Profile()
        {
            return View();
        }
    }
}
