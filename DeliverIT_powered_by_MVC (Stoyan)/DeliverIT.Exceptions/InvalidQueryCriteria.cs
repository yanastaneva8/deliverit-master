﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class InvalidQueryCriteria : Exception
    {
        public InvalidQueryCriteria()
        {
        }
        public InvalidQueryCriteria(string criteriaPropertyName, string className)
            : base(String.Format("Provided criteria {0} is not subproperty of any property of class {1}", criteriaPropertyName, className))
        {
        }
    }
}
