﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public static class ConstantStringMessages
    {
        // Address
        public readonly static string addressNotFound = "Address not found";

        // Customer 
        public readonly static string customerNotFound = "Customer not found";
        public readonly static string customerDeleted = "Customer deleted";

        // Shipment
        public readonly static string shipmentDispatched = "Shipment dispatched";
        public readonly static string shipmentReverted = "Shipment reverted";
        public readonly static string shipmentDeleted = "Shipment deleted";

        // Parcel
        public readonly static string parcelNotFound = "Parcel not found";
        public readonly static string parcelDeleted = "Parcel deleted";

        // Employee
        public readonly static string employeeNotFound = "Employee not found";
        public readonly static string employeeDeleted = "Employee deleted";

        // Warehouse
        public readonly static string warehouseDeleted = "Warehouse deleted";
        public readonly static string warehouseNotFound = "Warehouse not found";

        // Search
        public readonly static string criteriaNotSpecified = "Please specify search criteria string";

        // Authorization
        public readonly static string unauthorized = "You shall not pass!";
    }
}
