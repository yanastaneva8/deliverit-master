﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CannotUpdateStatus : Exception
    {
        public CannotUpdateStatus()
        {

        }

        public CannotUpdateStatus(string status, string requiredStatus) :
            base(String.Format("Cannot update status to {0}, status has to be {1}", status, requiredStatus))
        {
                
        }
    }
}
