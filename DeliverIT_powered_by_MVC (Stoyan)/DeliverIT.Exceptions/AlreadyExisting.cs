﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class AlreadyExisting : Exception
    {
        public AlreadyExisting()
        {
        }
        public AlreadyExisting(string itemType, string itemName)
            :base(String.Format("Object type {0} with name {1} already exists in the database", itemType, itemName))
        {
        }
    }
}
