﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class FailedToFindQuery : Exception
    {
        public FailedToFindQuery()
        {

        }

        public FailedToFindQuery(string propertyName, string itemType)
            : base(String.Format("Provided {0} is not a property of any {1}", propertyName, itemType))
        {

        }
    }
}
