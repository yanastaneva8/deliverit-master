﻿using System;

namespace DeliverIT.Exceptions
{
    public class NonExistentId : Exception
    {
        public NonExistentId()
        {

        }

        public NonExistentId(string item, int? id)
            : base(String.Format("Nonexistent {0} with id {1}", item, id))
        {

        }
    }
}
