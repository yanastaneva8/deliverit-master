﻿using System;

namespace DeliverIT.Exceptions
{
    public class StatusReached : Exception
    {
        public StatusReached()
        {

        }

        public StatusReached(string status)
            : base(String.Format("{0} status reached", status))
        {

        }
    }
}