﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class NonExistentName : Exception
    {
        public NonExistentName()
        {
        }

        public NonExistentName(string typeName, string instanceName)
            : base(String.Format("Nonexistent {0} {1}", typeName, instanceName))
        {
        }
    }
}
