﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class NoPendingItems : Exception
    {
        public NoPendingItems()
        {

        }

        public NoPendingItems(string item)
            : base(String.Format("No pending {0} to show", item))
        {

        }
    }
}
