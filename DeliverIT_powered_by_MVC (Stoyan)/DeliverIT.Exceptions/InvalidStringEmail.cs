﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class InvalidStringEmail : Exception
    {
        public InvalidStringEmail()
            : base(String.Format("Email must contain the /'@/' symbol"))
        {

        }
    }
}

