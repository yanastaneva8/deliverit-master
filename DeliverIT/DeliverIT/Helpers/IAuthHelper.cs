﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Web.Helpers
{
    public interface IAuthHelper
    {
        EmployeeDTO GetValidEmployee(string email);
        CustomerDTO GetValidCustomer(string email);

        bool DoesEmailBelongToCustomerId(string email, int customerId);
        bool DoesEmailBelongToEmployeeId(string email, int employeeId);
    }
}
