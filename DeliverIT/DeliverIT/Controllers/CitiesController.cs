﻿using DeliverIT.Services.Contracts;
using Microsoft.AspNetCore.Mvc;
using DeliverIT.Exceptions;
using DeliverIT.Web.Helpers;

namespace DeliverIT.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class CitiesController : ControllerBase
    {
        /// <summary>
        /// City service and auth helper fields
        /// </summary>
        private readonly ICityService cityService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with city service and auth helper injection
        /// </summary>
        /// <param name="cityService"></param>
        /// <param name="authHelper">Authentication helper</param>
        public CitiesController(ICityService cityService, IAuthHelper authHelper)
        {
            this.cityService = cityService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all cities
        /// </summary>
        /// <returns>All cities</returns>
        [HttpGet("")]
        public IActionResult Get([FromHeader] string email)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                var cities = this.cityService.Get();
                return this.Ok(cities);
            }
            return Unauthorized();

        }

        /// <summary>
        /// Get city by id
        /// </summary>
        /// <param name="id">City Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns>City with input Id</returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                {
                    var city = this.cityService.Get(id);
                    return this.Ok(city);
                }
                return Unauthorized();
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }

        }

        /// <summary>
        /// Create city
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="email">Email for authentication</param>
        /// <returns>New city</returns>
        [HttpPost()]
        public IActionResult Post([FromHeader] string email,
            [FromQuery] string cityName, 
            string countryName)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                {
                    var newCity = cityService.Post(cityName, countryName);
                    return this.Created("post", newCity);
                }
                return Unauthorized();
            }
            catch (NonExistentName e)
            {
                return NotFound(e.Message);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }
    }
}
