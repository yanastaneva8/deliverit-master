﻿using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Mvc;


namespace DeliverIT.Web.Controllers
{

    [ApiController]
    [Route("api/[controller]")]
    public class WarehousesController : ControllerBase
    {
        /// <summary>
        /// Warehouse service and auth helper fields
        /// </summary>
        private readonly IWarehouseService warehouseService;
        private readonly IAuthHelper authHelper;

        /// <summary>
        /// Ctor with warehouse service and auth helper injection
        /// </summary>
        /// <param name="warehouseService">Warehouse service</param>
        /// <param name="authHelper">Authentication helper</param>
        public WarehousesController(IWarehouseService warehouseService, 
            IAuthHelper authHelper)
        {
            this.warehouseService = warehouseService;
            this.authHelper = authHelper;
        }


        /// <summary>
        /// Get all warehouses
        /// </summary>
        /// <returns>All warehouses</returns>
        [HttpGet("")]
        public IActionResult Get()
        {
            var warehouses = this.warehouseService.Get();

            return Ok(warehouses);
        }

        /// <summary>
        /// Get warehouse by id
        /// </summary>
        /// <param name="id">Country Id</param>
        /// <returns>Warehouse with input id</returns>
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            try
            {
                var warehouse = this.warehouseService.Get(id);

                return Ok(warehouse);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get next shipment to arrive in warehouse
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        [HttpGet("{id}/next")]
        public IActionResult GetShipment([FromHeader] string email,
            int id)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    var shipment = this.warehouseService.GetNextShipment(id);

                    return Ok(shipment);
                }
                catch (NoPendingItems e)
                {
                    return NotFound(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Create warehouse
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpPost("")]
        public IActionResult Post([FromHeader] string email,
            [FromQuery] string cityName,
            string countryName,
            string streetName
            )
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    return Created("post", warehouseService.Post(cityName, countryName, streetName));
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
                catch (NonExistentName e)
                {
                    return NotFound(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Update warehouse properties
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <param name="newCountry">New country</param>
        /// <param name="newCity">New city</param>
        /// <param name="newStreetName">New street name</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string email,
            int id, [FromQuery]
            string newCountry,
            string newCity,
            string newStreetName)
        {

            if (this.authHelper.GetValidEmployee(email) != null)
            {

                try
                {
                    return this.Ok(warehouseService.Put(id, newCountry, newCity, newStreetName));
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
                catch (NonExistentName e)
                {
                    return NotFound(e.Message);
                }
                catch (AlreadyExisting e)
                {
                    return BadRequest(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Delete warehouse
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <param name="email">Email for authentication</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string email,
            int id)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                if (warehouseService.Delete(id))
                    return Ok(ConstantStringMessages.warehouseDeleted);

                return NotFound(ConstantStringMessages.warehouseNotFound);
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

    }

}
