﻿using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Web.Helpers;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Linq;


namespace DeliverIT.Web.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ShipmentsController : ControllerBase
    {
        /// <summary>
        /// Shipment service and auth helper fields
        /// </summary>
        IShipmentService shipmentService;
        IAuthHelper authHelper;

        /// <summary>
        /// Ctor with shipment service and auth helper injections
        /// </summary>
        /// <param name="shipmentService">Shipment service</param>
        /// <param name="authHelper">Authentication helper</param>
        public ShipmentsController(IShipmentService shipmentService,
            IAuthHelper authHelper)
        {
            this.shipmentService = shipmentService;
            this.authHelper = authHelper;
        }

        /// <summary>
        /// Get all shipments
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="customerId">Customer Id for filtration</param>
        /// <returns></returns>
        [HttpGet()]
        public IActionResult Get([FromHeader] string email,
            int? customerId)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                {
                    var shipments = shipmentService.GetMany(customerId).ToList();
                    return Ok(shipments);
                }

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Get shipment by id
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Shipment Id</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public IActionResult Get([FromHeader] string email,
            int id)
        {
            try
            {
                if (this.authHelper.GetValidEmployee(email) != null)
                {
                    return Ok(shipmentService.Get(id));
                }

                return Unauthorized(ConstantStringMessages.unauthorized);
            }
            catch (NonExistentId e)
            {
                return NotFound(e.Message);
            }
        }

        /// <summary>
        /// Create new shipment
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="departureDateString">Departure date - enter dd/mm/yyyy</param>
        /// <param name="arrivalDateString">Arrival date - enter dd/mm/yyyy</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="arrivalWarehouseId">Arrival wrehouse Id</param>
        /// <returns></returns>
        [HttpPost]
        public IActionResult Post([FromHeader] string email,
            [FromQuery] string departureDateString,
            string arrivalDateString,
            int departureWarehouseId,
            int arrivalWarehouseId)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    var shipment = shipmentService.Post(departureDateString,
                                    arrivalDateString,
                                    departureWarehouseId,
                                    arrivalWarehouseId);

                    return Created("post", shipment);
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Update shipment properties
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Shipment Id</param>
        /// <param name="action">Action - enter "revert" to revert status, "dispatch" to dispatch shipment, "receive" to receive shipment</param>
        /// <param name="travelTimeHrs">Travel time in hours</param>
        /// <param name="parcelIds">Int array of parcel Ids</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public IActionResult Put([FromHeader] string email,
            int id,
            [FromQuery] string action,
            int travelTimeHrs,
            [FromQuery] params int[] parcelIds)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    if (parcelIds != default)
                    {
                        shipmentService.AddParcelsToShipment(id, parcelIds);
                    }

                    if (action != default)
                    {
                        switch (action)
                        {
                            case ("dispatch"):
                                shipmentService.DispatchShipment(id, travelTimeHrs);
                                return Ok(ConstantStringMessages.shipmentDispatched);

                            case ("revert"):
                                shipmentService.RevertStatus(id);
                                return Ok(ConstantStringMessages.shipmentReverted);

                            case ("receive"):
                                shipmentService.ReceiveShipment(id);
                                return Ok("Shipment received");

                            default: throw new NonExistentName("command", "action");
                        }

                    }



                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }

                return Ok("No changes issued.");
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }

        /// <summary>
        /// Delete shipment
        /// </summary>
        /// <param name="email">Email for authentication</param>
        /// <param name="id">Shipment Id</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public IActionResult Delete([FromHeader] string email,
            int id)
        {
            if (this.authHelper.GetValidEmployee(email) != null)
            {
                try
                {
                    if (shipmentService.Delete(id))
                    {
                        return Ok(ConstantStringMessages.shipmentDeleted);
                    }
                }
                catch (NonExistentId e)
                {
                    return NotFound(e.Message);
                }
            }

            return Unauthorized(ConstantStringMessages.unauthorized);
        }
    }
}
