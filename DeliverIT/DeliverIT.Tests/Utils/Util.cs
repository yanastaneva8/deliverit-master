﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.Utils
{
    public static class Util
    {
        public static DbContextOptions<T> GetInMemoryDatabaseOptions<T>()
            where T : DbContext
        {
            var options = new DbContextOptionsBuilder<T>()
                .UseInMemoryDatabase("FakeDatabase")
                .Options;

            return options;
        }
        
        public static void SeedDatabase(DeliverITContext context)
        {
            context.Addresses.AddRange(
                new Address
                {
                    AddressId = 1,
                    CityId = 2,
                    StreetName = "Vitosha Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 2,
                    CityId = 1,
                    StreetName = "Sevastopol Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 3,
                    CityId = 2,
                    StreetName = "Bulgaria Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 4,
                    CityId = 3,
                    StreetName = "Knightsbridge",
                    CountryId = 2
                }
            );

            // Total 5
            context.Categories.AddRange(
            
                new Category
                {
                    CategoryId = 1,
                    Name = "Electronics, Computers"
                },
                new Category
                {
                    CategoryId = 2,
                    Name = "Books"
                },
                new Category
                {
                    CategoryId = 3,
                    Name = "Clothes, Shoes, Accessories"
                },
                new Category
                {
                    CategoryId = 4,
                    Name = "Beauty, Health, Grocery"
                },
                new Category
                {
                    CategoryId = 5,
                    Name = "Home, Garden, Pets"
                }
            );

            // Total 3 - 1 Vn, 2 Sf, 3 Ln
            context.Cities.AddRange(
                new City
                {
                    CityId = 1,
                    Name = "Varna",
                    CountryID = 1,
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia",
                    CountryID = 1,
                },
                new City
                {
                    CityId = 3,
                    Name = "London",
                    CountryID = 2,
                }
            );

            // Total 2 - 1 BG, 2 UK
            context.Countries.AddRange(
            
                new Country
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    CountryId = 2,
                    Name = "United Kingdom"
                }
            );

            // Total 3 
            context.Customers.AddRange(
            
                new Customer
                {
                    CustomerId = 1,
                    FirstName = "Yavor",
                    LastName = "Yanakiev",
                    Email = "100@kila.com",
                    DeliveryWarehouseId = 1
                },
                new Customer
                {
                    CustomerId = 2,
                    FirstName = "Dim4ou",
                    LastName = "Haralampiev",
                    Email = "dim4ou@dimaka.com",
                    DeliveryWarehouseId = 2
                },
                new Customer
                {
                    CustomerId = 3,
                    FirstName = "Yavor",
                    LastName = "Todorov",
                    Email = "qvkata@dlg.com",
                    DeliveryWarehouseId = 1
                }
            );

            // Total 2
            context.Employees.AddRange(
            
                new Employee
                {
                    EmployeeId = 1,
                    FirstName = "Stoyan",
                    LastName = "Kuklev",
                    Email = "stoyan@deliverit.com",
                    WarehouseId = 1
                },
                new Employee
                {
                    EmployeeId = 2,
                    FirstName = "Yana",
                    LastName = "Staneva",
                    Email = "yana@deliverit.com",
                    WarehouseId = 2
                }
            );

            // Total 4 - 1,2,3 in shipment 1, 4 in shipment 2
            context.Parcels.AddRange(
            
                new Parcel
                {
                    ParcelId = 1,
                    CustomerId = 1,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 2.5,
                    CategoryId = 1,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 2,
                    CustomerId = 2,
                    ArrivalWarehouseId = 2,
                    DepartureWarehouseId = 1,
                    Weight = 1.5,
                    CategoryId = 2,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 3,
                    CustomerId = 1,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 3.5,
                    CategoryId = 3,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 4,
                    CustomerId = 2,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 4.5,
                    CategoryId = 4,
                    ShipmentId = 2
                }
            );


            // Total 2 
            context.Shipments.AddRange(
            
                new Shipment
                {
                    ShipmentId = 1,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 1,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                },
                new Shipment
                {
                    ShipmentId = 2,
                    DepartureDate = DateTime.Now.AddDays(-1),
                    ArrivalDate = DateTime.Now,
                    StatusId = 1,
                    DepartureWarehouseId = 2,
                    ArrivalWarehouseId = 1
                }
            );

            // Total 3 - 1 Prep, 2 OTW, 3 Completed
            context.Statuses.AddRange(
            
                new Status
                {
                    StatusId = 1,
                    Name = "Preparing"
                },
                new Status
                {
                    StatusId = 2,
                    Name = "On the way"
                },
                new Status
                {
                    StatusId = 3,
                    Name = "Completed"
                }
            );

            // Total 4 - 1 Sf, 2 Vn, 3 Ln, 4 Sf
            context.Warehouses.AddRange(
                new Warehouse
                {
                    WarehouseId = 1,
                    AddressId = 1,
                    IsDeleted = false
                },
                new Warehouse
                {
                    WarehouseId = 2,
                    AddressId = 2,
                    IsDeleted = false
                },
                new Warehouse
                {
                    WarehouseId = 3,
                    AddressId = 4,
                    IsDeleted = false
                },
                new Warehouse
                {
                    WarehouseId = 4,
                    AddressId = 3,
                    IsDeleted = false
                }
            );
        }

        
    }
}
