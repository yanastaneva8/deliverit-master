﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var newEmployeeService = new EmployeeService(context);

                // Act
                var testEmployee = new Employee()
                {
                    FirstName = "Pesho",
                    LastName = "Admina",
                    Email = "peshkata@deliverit.com",
                    EmployeeId = 100,
                    IsDeleted = false,
                    WarehouseId = 1
                };

                context.Employees.Add(testEmployee);
                context.SaveChanges();

                // Assert
                Assert.IsInstanceOfType(newEmployeeService.Get(100), typeof(EmployeeDTO));
                Assert.IsTrue(newEmployeeService.Delete(100));
                Assert.ThrowsException<NonExistentId>(() => newEmployeeService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
