﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var newEmployeeService = new EmployeeService(context);

                // Act
                var sut = newEmployeeService.Post("Pesho", "Admina", "peshkata@deliverit.com", 1);

                // Assert
                Assert.IsInstanceOfType(sut, typeof(EmployeeDTO));

                context.Database.EnsureDeleted();
            }
        }
    }
}
