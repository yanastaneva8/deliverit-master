﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var newEmployeeService = new EmployeeService(context);

                //Act & Assert
                Assert.IsInstanceOfType(newEmployeeService, typeof(EmployeeService));
            }
        }

    }
}
