﻿using System.Linq;
using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.EmployeeServiceTests
{
    [TestClass]
    public class GetByEmail_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var newEmployeeService = new EmployeeService(context);

                var employee = new Employee
                {
                    FirstName = "Gosho",
                    LastName = "Admina",
                    Email = "goshoadmina@deliverit.com",
                    EmployeeId = 100,
                    IsDeleted = false,
                    WarehouseId = 4
                };

                context.Employees.Add(employee);
                context.SaveChanges();

                // Assert
                Assert.AreEqual(newEmployeeService.GetByEmail("goshoadmina@deliverit.com").Email, employee.Email);
                Assert.ThrowsException<NonExistentName>(() => newEmployeeService.GetByEmail("nonexistent"));
                context.Database.EnsureDeleted();

            }
        }

    }
}
