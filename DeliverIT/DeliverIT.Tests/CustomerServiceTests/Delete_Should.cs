﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.CustomerServiceTests
{
    [TestClass]
    public class Delete_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var newCustomerService = new CustomerService(context);

                var customer = new Customer
                {
                    FirstName = "Gosho",
                    LastName = "Customera",
                    Email = "goshocustomera@deliverit.com",
                    CustomerId = 100,
                    IsDeleted = false,
                    DeliveryWarehouseId = 4
                };

                context.Customers.Add(customer);
                context.SaveChanges();

                // Assert
                Assert.IsInstanceOfType(newCustomerService.Get(100), typeof(CustomerDTO));
                Assert.IsTrue(newCustomerService.Delete(100));
                Assert.ThrowsException<NonExistentId>(() => newCustomerService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
