﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class Put_Should
    {
        [TestMethod]
        public void EditCorrectlyEachPropertyOfObject()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();
                var addressServiceMock = new Mock<IAddressService>();

                warehouseServiceMock.Setup(x => x.DbGetOne(1)).Returns(context.Warehouses.Where(w => !w.IsDeleted)
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .FirstOrDefault(w => w.WarehouseId == 1)
                ?? throw new NonExistentId("warehouse", 1));

                warehouseServiceMock.Setup(x => x.DbGetOne(2)).Returns(context.Warehouses.Where(w => !w.IsDeleted)
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .FirstOrDefault(w => w.WarehouseId == 2)
                ?? throw new NonExistentId("warehouse", 2));

                shipmentServiceMock.Setup(x=>x.DbGetOne(2)).Returns(context.Shipments
                .Include(s => s.DepartureWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.City)
                 .Include(s => s.DepartureWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.City)
                 .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Status)
                .FirstOrDefault(s => s.ShipmentId == 2 && !s.IsDeleted)
                ?? throw new NonExistentId("shipment", 2));

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                var testParcel = new Parcel() { ParcelId = 99, CustomerId=1,CategoryId=2, ArrivalWarehouseId = 1, DepartureWarehouseId = 2, ShipmentId = 1 };
                context.Parcels.Add(testParcel);

                context.SaveChanges();

                //Act
                var sut = newParcelService.Put(99, 2, 1, 2);
                

                //Assert
                Assert.AreEqual(sut.ParcelId, 99);
                Assert.AreEqual(sut.ArrivalWarehouseId, 2);
                Assert.AreEqual(sut.DepartureWarehouseId, 1);
                Assert.AreEqual(sut.CategoryId, 2);
                Assert.AreEqual(sut.ShipmentId, 2);

                context.Database.EnsureDeleted();
            }

        }
    }
}
