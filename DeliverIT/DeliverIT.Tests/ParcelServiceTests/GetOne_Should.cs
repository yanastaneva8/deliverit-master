﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Services.Contracts;
using DeliverIT.Exceptions;

namespace DeliverIT.Tests.ParcelServiceTests
{
    [TestClass]
    public class GetOne_Should
    {
        [TestMethod]
        public void ProperlyGetTheRequiredParcel()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act 

                var parcel = new Parcel()
                {
                    ParcelId = 99,
                    CustomerId = 1,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 2.5,
                    CategoryId = 1,
                    ShipmentId = 1
                };

                context.Parcels.Add(parcel);
                context.SaveChanges();


                //Assert
                Assert.IsTrue(newParcelService.Get(99).ParcelId == parcel.ParcelId);


                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowWhenInvalidParamIsPassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var warehouseServiceMock = new Mock<IWarehouseService>();
                var customerServiceMock = new Mock<ICustomerService>();
                var shipmentServiceMock = new Mock<IShipmentService>();

                var newParcelService = new ParcelService(context, warehouseServiceMock.Object, customerServiceMock.Object, shipmentServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(()=>newParcelService.Get(99));

                context.Database.EnsureDeleted();
            }

        }
    }
}