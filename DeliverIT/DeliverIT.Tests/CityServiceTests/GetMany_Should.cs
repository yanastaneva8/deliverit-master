﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.CityServiceTests
{
    [TestClass]
    public class GetMany_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);

                // Act
                Assert.AreEqual(newCityService.Get().Count(), context.Cities.Count());
                context.Database.EnsureDeleted();
            }
        }
    }
}
