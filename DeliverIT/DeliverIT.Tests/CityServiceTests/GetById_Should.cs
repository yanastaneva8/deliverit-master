﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.CityServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);


                var city = new City
                {
                    CityId = 1,
                    CountryID = 1,
                    Name = "Varna"
                };


                // Assert
                Assert.AreEqual(newCityService.Get(1).Name, city.Name);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowNonexistentId_City()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);

                // Assert
                Assert.ThrowsException<NonExistentId>(() => newCityService.Get(100));
                context.Database.EnsureDeleted();

            }
        }

        [TestMethod]
        public void ThrowNonexistentName_City()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                context.SaveChanges();

                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);

                // Assert
                Assert.ThrowsException<NonExistentName>(() => newCityService.Get("non existent name"));
                context.Database.EnsureDeleted();

            }
        }
    }
}
