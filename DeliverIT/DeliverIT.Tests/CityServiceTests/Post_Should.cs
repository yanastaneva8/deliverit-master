﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.CityServiceTests
{
    [TestClass]

    public class Post_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();
                var countryServiceMock = new Mock<ICountryService>();          

                var newCityService = new CityService(context, countryServiceMock.Object);

                // Act
                var sut = newCityService.Post("Plovdiv", "Bulgaria");

                // Assert
                Assert.IsInstanceOfType(sut, typeof(CityDTO));
                Assert.AreEqual(sut.Name, "Plovdiv");


                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowsAlreadyExisting_City()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);
                context.SaveChanges();

                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);


                // Act & Assert
                Assert.ThrowsException<AlreadyExisting>(() => newCityService.Post("Sofia", "Bulgaria"));

                context.Database.EnsureDeleted();
            }
        }
    }
}
