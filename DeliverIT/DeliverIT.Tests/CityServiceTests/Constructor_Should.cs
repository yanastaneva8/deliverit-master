﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.CityServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var countryServiceMock = new Mock<ICountryService>();

                var newCityService = new CityService(context, countryServiceMock.Object);

                //Act & Assert
                Assert.IsInstanceOfType(newCityService, typeof(CityService));
            }
        }

    }
}
