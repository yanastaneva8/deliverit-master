﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;

namespace DeliverIT.Tests.CountryServiceTests
{
    [TestClass]
    public class GetMany_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();
                Util.SeedDatabase(context);

                var newCountryService = new CountryService(context);

                // Act
                Assert.AreEqual(newCountryService.GetAll().Count(), context.Countries.Count());
                context.Database.EnsureDeleted();
            }
        }
    }
}
