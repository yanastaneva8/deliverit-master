﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.AddressServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var cityServiceMock = new Mock<ICityService>();
                var countryServiceMock = new Mock<ICountryService>();

                var newAddressService = new AddressService(context, cityServiceMock.Object, countryServiceMock.Object);

                //Act & Assert
                Assert.IsInstanceOfType(newAddressService, typeof(AddressService));
            }
        }

    }
}
