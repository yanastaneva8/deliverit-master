﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class Post_Should
    {
        [TestMethod]
        public void CreateProperObjectIfProvidedWithValidParams()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                warehouseServiceMock.Setup(x => x.DbGetAll()).Returns(context.Warehouses
                .Where(w => !w.IsDeleted)
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country));

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);
                
                //Act
                var sut = testShipmentService.Post("12/12/2021", "15/12/2021", 3, 4);
                var comparerOg = context.Shipments.ToList().TakeLast(1).FirstOrDefault();
                var comparerDto = new ShipmentDTO(comparerOg);

                //Assert


                Assert.AreEqual(sut.ShipmentId, comparerDto.ShipmentId);
                Assert.AreEqual(sut.ArrivalDate, comparerDto.ArrivalDate);
                Assert.AreEqual(sut.DepartureDate, comparerDto.DepartureDate);
                Assert.AreEqual(sut.ArrivalWarehouse.Warehouse, comparerDto.ArrivalWarehouse.Warehouse);
                Assert.AreEqual(sut.DepartureWarehouse.Warehouse, comparerDto.DepartureWarehouse.Warehouse);
                Assert.AreEqual(sut.ParcelsIds, comparerDto.ParcelsIds);
                Assert.AreEqual(sut.CustomersFor, comparerDto.CustomersFor);
                Assert.AreEqual(sut.Status.Name, comparerDto.Status.Name);


                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfDatesAreInconsistent()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);


                //Act & Assert
                Assert.ThrowsException<InvalidInputTime>(() => testShipmentService.Post("12/12/2021", "10/12/2021", 3, 4));

                context.Database.EnsureDeleted();
            }
        }

    }
}
