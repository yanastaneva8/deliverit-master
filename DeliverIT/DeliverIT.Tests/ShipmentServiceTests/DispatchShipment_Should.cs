﻿using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Data;
using DeliverIT.Data.Models;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class DispatchShipment_Should
    {
        [TestMethod]
        public void ProperlyMarkShipmentAsDispatched()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 1,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2,
                    Parcels = new List<Parcel>()
                    {
                        new Parcel
                        {
                            ParcelId = 99,
                            CustomerId = 1,
                            ArrivalWarehouseId = 1,
                            DepartureWarehouseId = 2,
                            Weight = 2.5,
                            CategoryId = 1,
                            ShipmentId = 99
                        }
                    }

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var sut = context.Shipments.FirstOrDefault(sh => sh.ShipmentId == 99);
                testShipmentService.DispatchShipment(99, 96);

                //Assert
                Assert.AreEqual(sut.StatusId, 2);



                context.Database.EnsureDeleted();
            }
        }


        [DataRow(99, 1, 1, 2, 99)]
        [TestMethod]
        public void ProperlyPopulateTheParcelWithProperties(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,
                    Parcels = new List<Parcel>()
                    {
                        new Parcel
                        {
                            ParcelId = parcelId,
                            CustomerId = 1,
                            ArrivalWarehouseId = arrivalWarehouseId,
                            DepartureWarehouseId = departureWarehouseId,
                            Weight = 2.5,
                            CategoryId = 1,
                            ShipmentId = shipmentId
                        }
                    }

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                testShipmentService.DispatchShipment(shipmentId, 96);

                var sut = context.Shipments.FirstOrDefault(sh => sh.ShipmentId == shipmentId);

                //Assert
                Assert.AreEqual(sut.ShipmentId, shipmentId);
                Assert.AreEqual(sut.ArrivalWarehouseId, arrivalWarehouseId);
                Assert.AreEqual(sut.DepartureWarehouseId, departureWarehouseId);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfInvalidShipmentIdIsPassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<NonExistentId>(() => testShipmentService.DispatchShipment(99, 96));

            }
        }

        [DataRow(99, 1, 1, 2)]
        [TestMethod]
        public void ThrowIfParcelCollectionIsEmpty(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,
                    Parcels = new List<Parcel>()

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act

                var sut = context.Shipments.FirstOrDefault(sh => sh.ShipmentId == shipmentId);

                //Act & Assert
                Assert.ThrowsException<CannotBeEmpty>(() => testShipmentService.DispatchShipment(shipmentId, 96));
            }
        }

        [DataRow(99, 2, 1, 2, 99)]
        [TestMethod]
        public void ThrowIfStatusNotOkForDispatching(int shipmentId, int statusId, int departureWarehouseId, int arrivalWarehouseId, int parcelId)
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = shipmentId,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = statusId,
                    DepartureWarehouseId = departureWarehouseId,
                    ArrivalWarehouseId = arrivalWarehouseId,
                    Parcels = new List<Parcel>()
                    {
                        new Parcel
                        {
                            ParcelId = parcelId,
                            CustomerId = 1,
                            ArrivalWarehouseId = arrivalWarehouseId,
                            DepartureWarehouseId = departureWarehouseId,
                            Weight = 2.5,
                            CategoryId = 1,
                            ShipmentId = shipmentId
                        }
                    }

                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<CannotUpdateStatus>(() => testShipmentService.DispatchShipment(shipmentId, 96));

            }
        }




    }
}
