﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class Counstructor_Should
    {
        [TestMethod]
        public void InstantiateProperShipmentObject()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);
                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                //Act
                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Assert
                Assert.IsInstanceOfType(testShipmentService, typeof(ShipmentService));

                context.Database.EnsureDeleted();
            }
        }
    }
}
