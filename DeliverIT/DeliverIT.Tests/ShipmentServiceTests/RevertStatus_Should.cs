﻿using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Data;
using DeliverIT.Data.Models;

namespace DeliverIT.Tests.ShipmentServiceTests
{
    [TestClass]
    public class RevertStatus_Should
    {
        [TestMethod]
        public void ProperlyRevertStatusOfAShipmentIfValidParamsArePassed()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 2,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act
                var sut = shipmentEntry.Entity;
                testShipmentService.RevertStatus(99);

                //Assert
                Assert.AreEqual(sut.StatusId, 1);

                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfStatusIsMaxedAndCantGoBack()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                context.Database.EnsureDeleted();

                Util.SeedDatabase(context);

                var shipmentEntry = context.Shipments.Add(new Shipment
                {
                    ShipmentId = 99,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 3,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                });

                context.SaveChanges();

                var warehouseServiceMock = new Mock<IWarehouseService>();

                var testShipmentService = new ShipmentService(context, warehouseServiceMock.Object);

                //Act & Assert
                Assert.ThrowsException<StatusReached>(() => testShipmentService.RevertStatus(99));

                context.Database.EnsureDeleted();
            }
        }
    }
}
