﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class GetNextShipment_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);
                var shipment = new Shipment
                {
                    ShipmentId = 99,
                    StatusId = 2,
                    ArrivalDate = DateTime.Now,
                    ArrivalWarehouseId = 1,
                    DepartureDate = DateTime.Now,
                    DepartureWarehouseId = 2,
                    IsDeleted = false
                };

                context.Shipments.Add(shipment);
                context.SaveChanges();


                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);


                // Assert
                Assert.AreEqual(newWarehouseService.GetNextShipment(1).StatusId, shipment.StatusId);
                Assert.AreEqual(newWarehouseService.GetNextShipment(1).ShipmentId, shipment.ShipmentId);
                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowNoPendingItems()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);
                var shipment = new Shipment
                {
                    ShipmentId = 99,
                    StatusId = 2,
                    ArrivalDate = DateTime.Now,
                    ArrivalWarehouseId = 1,
                    DepartureDate = DateTime.Now,
                    DepartureWarehouseId = 2,
                    IsDeleted = false
                };

                context.Shipments.Add(shipment);
                context.SaveChanges();


                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);


                // Assert
                Assert.AreEqual(newWarehouseService.GetNextShipment(1).StatusId, shipment.StatusId);
                Assert.AreEqual(newWarehouseService.GetNextShipment(1).ShipmentId, shipment.ShipmentId);
                context.Database.EnsureDeleted();
            }
        }
    }
}
