﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class Get_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {

                context.Database.EnsureDeleted();

                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);


                // Assert
                Assert.AreEqual(newWarehouseService.Get().Count(), context.Warehouses.Count());
                context.Database.EnsureDeleted();
            }
        }

       
    }
}
