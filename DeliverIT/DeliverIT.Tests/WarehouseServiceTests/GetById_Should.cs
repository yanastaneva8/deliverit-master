﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class GetById_Should
    {
        [TestMethod]
        public void ExecuteCorrectly()
        {
            //Arrange
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);
                context.SaveChanges();

                var addressService = new Mock<IAddressService>();


                var sut = new WarehouseService(context, addressService.Object);

               

                // Assert
                Assert.IsInstanceOfType(sut.Get(1), typeof(WarehouseDTO));
                context.Database.EnsureDeleted();
            }
        }

        [TestMethod]
        public void ThrowIfNonexistentId()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                Util.SeedDatabase(context);

                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);


                // Act & Assert
                Assert.ThrowsException<NonExistentId>(() => newWarehouseService.Get(100));

                context.Database.EnsureDeleted();
            }
        }
    }
}
