﻿using DeliverIT.Data;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.Services;
using DeliverIT.Tests.Utils;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DeliverIT.Tests.WarehouseServiceTests
{
    [TestClass]
    public class Constructor_Should
    {
        [TestMethod]
        public void InstantiateProperly()
        {
            //Arrange
            var options = Util.GetInMemoryDatabaseOptions<DeliverITContext>();

            using (var context = new DeliverITContext(options))
            {
                var addressService = new Mock<IAddressService>();

                var newWarehouseService = new WarehouseService(context, addressService.Object);

                //Act & Assert
                Assert.IsInstanceOfType(newWarehouseService, typeof(WarehouseService));
            }
        }

    }
}
