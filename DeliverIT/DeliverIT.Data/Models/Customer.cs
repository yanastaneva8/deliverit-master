﻿using System;
using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;
using System.Net.Mail;
using System.Collections.Generic;

namespace DeliverIT.Data.Models
{
    public class Customer
    {
        /// <summary>
        /// Customer fields for first and last names, and email
        /// </summary>
        private string firstName;
        private string lastName;
        private string email;

        /// <summary>
        /// Id property, primary key of the customers table
        /// </summary>
        [Key]
        public int CustomerId { get; set; }

        /// <summary>
        /// FirstName property, validation included
        /// </summary>
        [Required]
        [MinLength(1), MaxLength(50)]
        public string FirstName 
        {
            get
            {
                return this.firstName;
            }
            set
            {
                int min = 1;
                int max = 50;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("First name", min, max);
                }

                this.firstName = value;
            }
        }
        
        /// <summary>
        /// LastName property, validation included
        /// </summary>
        [Required]
        [MinLength(1), MaxLength(50)]
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                int min = 1;
                int max = 50;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Last name", min, max);
                }

                this.lastName = value;
            }
        }

        /// <summary>
        /// Email property, validation included
        /// </summary>
        [Required]
        public string Email 
        { 
            get
            {
                return this.email;
            }
            set
            {
                int min = 2;
                int max = 100;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Email", min, max);
                }

                try
                {
                    var mail = new MailAddress(value);
                    this.email = mail.Address;
                }
                catch (FormatException)
                {
                    throw new InvalidStringEmail();
                }
            }
        }

        /// <summary>
        /// DeliveryWarehouseId property, foreign key of addresses table
        /// </summary>
        [Required]
        public int DeliveryWarehouseId { get; set; }

        /// <summary>
        /// DeliveryWarehouse property
        /// </summary>
        public Warehouse DeliveryWarehouse { get; set; }

        /// <summary>
        /// List containing all parcels for this customer
        /// </summary>
        public List<Parcel> Parcels { get; set; } = new List<Parcel>();

        /// <summary>
        /// Boolean property IsDeleted - false by default; true if customer deleted
        /// </summary>
        public bool IsDeleted { get; set; } = false;

    }
}
