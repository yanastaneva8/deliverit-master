﻿using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Address
    {
        /// <summary>
        /// Address field for street name
        /// </summary>
        private string streetName;

        /// <summary>
        /// Id property, primary key of addresses table
        /// </summary>
        [Key]
        public int AddressId { get; set; }

        /// <summary>
        /// CityId property, foreign key of cities table
        /// </summary>
        [Required]
        public int CityId { get; set; }

        /// <summary>
        /// City property
        /// </summary>
        public City City { get; set; }

        /// <summary>
        /// CountryId property, foreign key of countries table
        /// </summary>
        [Required]
        public int CountryId { get; set; }

        /// <summary>
        /// Country property
        /// </summary>
        public Country Country { get; set; }

        /// <summary>
        /// StreetName property, validation included
        /// </summary>
        [Required]
        [MinLength(3), MaxLength(30)]
        public string StreetName 
        {
            get
            {
                return this.streetName;
            }
            set
            {
                int min = 3;
                int max = 30;

                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Street name", min, max);
                }
                this.streetName = value;
            }
        }
    }
}
