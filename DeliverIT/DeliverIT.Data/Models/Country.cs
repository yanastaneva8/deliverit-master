﻿using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Country
    {
        /// <summary>
        /// Country fields for id and name
        /// </summary>
        private int id;
        private string name;

        /// <summary>
        /// Id property, primary key of countries table
        /// </summary>
        [Key]
        [Required]
        public int CountryId
        {
            get
            {
                return this.id;
            }
            set
            {
                if (value < 0)
                {
                    throw new CannotBeNegative("Id", "int");
                }
                this.id = value;
            }
        }

        /// <summary>
        /// Country name, validation included
        /// </summary>
        [Required]
        public string Name 
        {
            get
            {
                return this.name;
            }
            set
            {
                int min = 0; // There are cities in the world whose name are 1 letter long :)
                int max = 58; // Llanfair­pwllgwyngyll­gogery­chwyrn­drobwll­llan­tysilio­gogo­goch 
                              // holds the world record :D

                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("City name", min, max);
                }
                this.name = value;
            }
        }
    }
}
