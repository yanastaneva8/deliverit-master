﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Warehouse
    {
        /// <summary>
        /// Id property, primary key of warehouses table, validation included
        /// </summary>
        [Key]
        public int WarehouseId { get; set; }

        /// <summary>
        /// AddressId property, foreign key of addresses table
        /// </summary>
        [Required]
        public int AddressId { get; set; }

        /// <summary>
        /// Address property
        /// </summary>
        [Required]
        public Address Address { get; set; }

        /// <summary>
        /// Boolean property IsDeleted - false by default; true if warehouse deleted
        /// </summary>
        public bool IsDeleted { get; set; } = false;

    }
}
