﻿using System;
using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;
using System.Net.Mail;

namespace DeliverIT.Data.Models
{
    public class Employee
    {
        /// <summary>
        /// Employee fields for first and last names, and email
        /// </summary>
        private string firstName;
        private string lastName;
        private string email;

        /// <summary>
        /// Id property, primary key of the employees table
        /// </summary>
        [Key]
        public int EmployeeId { get; set; }

        /// <summary>
        /// FirstName property, validation included
        /// </summary>
        [Required]
        [MinLength(1), MaxLength(50)]
        public string FirstName
        {
            get
            {
                return this.firstName;
            }
            set
            {
                int min = 1;
                int max = 50;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("First name", min, max);
                }

                this.firstName = value;
            }

        }

        /// <summary>
        /// LastName property, validation included
        /// </summary>
        [Required]
        [MinLength(1), MaxLength(50)]
        public string LastName
        {
            get
            {
                return this.lastName;
            }
            set
            {
                int min = 1;
                int max = 50;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Last name", min, max);
                }

                this.lastName = value;
            }
        }

        /// <summary>
        /// Email property, validation included
        /// </summary>
        [Required]
        public string Email
        {
            get
            {
                return this.email;
            }
            set
            {
                int min = 2;
                int max = 100;
                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Email", min, max);
                }

                try
                {
                    var mail = new MailAddress(value);
                    this.email = mail.Address;
                }
                catch (FormatException)
                {
                    throw new InvalidStringEmail();
                }
            }
        }

        /// <summary>
        /// WarehouseId property, foreign key of warehouses table
        /// </summary>
        [Required]
        public int WarehouseId { get; set; }

        /// <summary>
        /// Warehouse property
        /// </summary>
        public Warehouse Warehouse { get; set; }

        /// <summary>
        /// Boolean property IsDeleted - false by default; true if employee deleted
        /// </summary>
        public bool IsDeleted { get; set; } = false;

    }
}
