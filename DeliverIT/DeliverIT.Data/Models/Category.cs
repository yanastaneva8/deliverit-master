﻿using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class Category
    {
        /// <summary>
        /// Category field for name
        /// </summary>
        private string name;

        /// <summary>
        /// Id property, primary key of categories table
        /// </summary>
        [Key]
        public int CategoryId { get; set; }
        
        /// <summary>
        /// Category name property, validation included
        /// </summary>
        [Required]
        [MinLength(3), MaxLength(50)]
        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                int min = 3;
                int max = 50;

                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("Category name", min, max);
                }
                this.name = value;
            }
        }

    }
}
