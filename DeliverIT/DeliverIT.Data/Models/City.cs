﻿using System.ComponentModel.DataAnnotations;
using DeliverIT.Exceptions;

namespace DeliverIT.Data.Models
{
    public class City
    {
        /// <summary>
        /// City field for name 
        /// </summary>
        private string name;

        /// <summary>
        /// Id property, primary key of cities table
        /// </summary>
        [Key]
        public int CityId { get; set; }

        /// <summary>
        /// Name property, validation included
        /// </summary>
        [Required]
        [MinLength(0), MaxLength(58)]
        public string Name 
        {
            get
            {
                return this.name;
            }
            set
            {
                int min = 0; // There are cities in the world whose name are 1 letter long :)
                int max = 58; // Llanfair­pwllgwyngyll­gogery­chwyrn­drobwll­llan­tysilio­gogo­goch 
                              // holds the world record :D

                if (value.Length < min || value.Length > max)
                {
                    throw new InvalidStringLength("City name", min, max);
                }
                this.name = value;
            }
        }

        /// <summary>
        /// CountryId property, foreign key of countries table
        /// </summary>
        [Required]
        public int CountryID { get; set; }
        
        /// <summary>
        /// Country property
        /// </summary>
        public Country Country { get; set; }
    }
}
