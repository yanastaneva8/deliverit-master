﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DeliverIT.Data.Models;
using Microsoft.EntityFrameworkCore;


namespace DeliverIT.Data
{
    public class DeliverITContext : DbContext
    {
        public DeliverITContext(DbContextOptions<DeliverITContext> options)
            : base(options)
        {
        }

        public DbSet<Address> Addresses { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Parcel> Parcels { get; set; }
        public DbSet<Shipment> Shipments { get; set; }
        public DbSet<Status> Statuses { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            // Address --------------------------------------------
            builder.Entity<Address>()
                .HasOne(a => a.Country)
                .WithMany()
                .HasForeignKey(a => a.CountryId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Address>()
               .HasOne(a => a.City)
               .WithMany()
               .HasForeignKey(a => a.CityId)
               .OnDelete(DeleteBehavior.Restrict);

            // Customer --------------------------------------------
            builder.Entity<Customer>()
                .HasOne(c => c.DeliveryWarehouse)
                .WithMany()
                .HasForeignKey(c => c.DeliveryWarehouseId)
                .OnDelete(DeleteBehavior.Restrict);
           

            // Employee --------------------------------------------
            builder.Entity<Employee>()
                .HasOne(e => e.Warehouse)
                .WithMany()
                .HasForeignKey(e => e.WarehouseId)
                .OnDelete(DeleteBehavior.Restrict);


            // Parcel --------------------------------------------
            builder.Entity<Parcel>()
               .HasOne(p => p.Category)
               .WithMany()
               .HasForeignKey(c => c.CategoryId)
               .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Parcel>()
               .HasOne(p => p.Customer)
               .WithMany(c=>c.Parcels)
               .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Parcel>()
               .HasOne(p => p.ArrivalWarehouse)
               .WithMany()
               .HasForeignKey(c => c.ArrivalWarehouseId)
               .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Parcel>()
               .HasOne(p => p.DepartureWarehouse)
               .WithMany()
               .HasForeignKey(c => c.DepartureWarehouseId)
               .OnDelete(DeleteBehavior.Restrict);

            //builder.Entity<Parcel>()
            //    .HasOne(p => p.Shipment)
            //    .WithMany()
            //    .HasForeignKey(s => s.ShipmentId)
            //    .IsRequired(false)
            //    .OnDelete(DeleteBehavior.Restrict);


            // Shipment --------------------------------------------
            builder.Entity<Shipment>()
                .HasOne(p => p.ArrivalWarehouse)
                .WithMany()
                .HasForeignKey(c => c.ArrivalWarehouseId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.Entity<Shipment>()
               .HasOne(p => p.DepartureWarehouse)
               .WithMany()
               .HasForeignKey(c => c.DepartureWarehouseId)
               .OnDelete(DeleteBehavior.Restrict);


            // Total 4 - 1 Sf, 2 Vn, 3 Sf, 4 Ln
            var addresses = new List<Address>()
            {
                new Address
                {
                    AddressId = 1,
                    CityId = 2,
                    StreetName = "Vitosha Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 2,
                    CityId = 1,
                    StreetName = "Sevastopol Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 3,
                    CityId = 2,
                    StreetName = "Bulgaria Boulevard",
                    CountryId = 1
                },
                new Address
                {
                    AddressId = 4,
                    CityId = 3,
                    StreetName = "Knightsbridge",
                    CountryId = 2
                }
            };

            // Total 5
            var categories = new List<Category>
            {
                new Category
                {
                    CategoryId = 1,
                    Name = "Electronics, Computers"
                },
                new Category
                {
                    CategoryId = 2,
                    Name = "Books"
                },
                new Category
                {
                    CategoryId = 3,
                    Name = "Clothes, Shoes, Accessories"
                },
                new Category
                {
                    CategoryId = 4,
                    Name = "Beauty, Health, Grocery"
                },
                new Category
                {
                    CategoryId = 5,
                    Name = "Home, Garden, Pets"
                }
            };

            // Total 3 - 1 Vn, 2 Sf, 3 Ln
            var cities = new List<City>
            {
                new City
                {
                    CityId = 1,
                    Name = "Varna",
                    CountryID = 1,
                },
                new City
                {
                    CityId = 2,
                    Name = "Sofia",
                    CountryID = 1,
                },
                new City
                {
                    CityId = 3,
                    Name = "London",
                    CountryID = 2,
                }
            };

            // Total 2 - 1 BG, 2 UK
            var countries = new List<Country>
            {
                new Country
                {
                    CountryId = 1,
                    Name = "Bulgaria"
                },
                new Country
                {
                    CountryId = 2,
                    Name = "United Kingdom"
                }
            };

            // Total 3 
            var customers = new List<Customer>
            {
                new Customer
                {
                    CustomerId = 1,
                    FirstName = "Yavor",
                    LastName = "Yanakiev",
                    Email = "100@kila.com",
                    DeliveryWarehouseId = 1
                },
                new Customer
                {
                    CustomerId = 2,
                    FirstName = "Dim4ou",
                    LastName = "Haralampiev",
                    Email = "dim4ou@dimaka.com",
                    DeliveryWarehouseId = 2
                },
                new Customer
                {
                    CustomerId = 3,
                    FirstName = "Yavor",
                    LastName = "Todorov",
                    Email = "qvkata@dlg.com",
                    DeliveryWarehouseId = 1
                }
            };

            // Total 2
            var employees = new List<Employee>
            {
                new Employee
                {
                    EmployeeId = 1,
                    FirstName = "Stoyan",
                    LastName = "Kuklev",
                    Email = "stoyan@deliverit.com",
                    WarehouseId = 1
                },
                new Employee
                {
                    EmployeeId = 2,
                    FirstName = "Yana",
                    LastName = "Staneva",
                    Email = "yana@deliverit.com",
                    WarehouseId = 2
                }
            };

            // Total 2 
            var shipments = new List<Shipment>
            {
                new Shipment
                {
                    ShipmentId = 1,
                    DepartureDate = DateTime.Now,
                    ArrivalDate = DateTime.Now.AddDays(2),
                    StatusId = 1,
                    DepartureWarehouseId = 1,
                    ArrivalWarehouseId = 2
                },
                new Shipment
                {
                    ShipmentId = 2,
                    DepartureDate = DateTime.Now.AddDays(-1),
                    ArrivalDate = DateTime.Now,
                    StatusId = 1,
                    DepartureWarehouseId = 2,
                    ArrivalWarehouseId = 1
                }
            };

            // Total 4 - 1,2,3 in shipment 1, 4 in shipment 2
            var parcels = new List<Parcel>
            {
                new Parcel
                {
                    ParcelId = 1,
                    CustomerId = 1,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 2.5,
                    CategoryId = 1,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 2,
                    CustomerId = 2,
                    ArrivalWarehouseId = 2,
                    DepartureWarehouseId = 1,
                    Weight = 1.5,
                    CategoryId = 2,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 3,
                    CustomerId = 1,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 3.5,
                    CategoryId = 3,
                    ShipmentId = 1
                },
                new Parcel
                {
                    ParcelId = 4,
                    CustomerId = 2,
                    ArrivalWarehouseId = 1,
                    DepartureWarehouseId = 2,
                    Weight = 4.5,
                    CategoryId = 4,
                    ShipmentId = 2
                }
            };


           

            // Total 3 - 1 Prep, 2 OTW, 3 Completed
            var statuses = new List<Status>
            {
                new Status
                {
                    StatusId = 1,
                    Name = "Preparing"
                },
                new Status
                {
                    StatusId = 2,
                    Name = "On the way"
                },
                new Status
                {
                    StatusId = 3,
                    Name = "Completed"
                }
            };

            // Total 4 - 1 Sf, 2 Vn, 3 Ln, 4 Sf
            var warehouses = new List<Warehouse>
            {
                new Warehouse
                {
                    WarehouseId = 1,
                    AddressId = 1,
                },
                new Warehouse
                {
                    WarehouseId = 2,
                    AddressId = 2,
                },
                new Warehouse
                {
                    WarehouseId = 3,
                    AddressId = 4,
                },
                new Warehouse
                {
                    WarehouseId = 4,
                    AddressId = 3
                }
            };



            builder.Entity<Address>().HasData(addresses);
            builder.Entity<Category>().HasData(categories);
            builder.Entity<City>().HasData(cities);
            builder.Entity<Country>().HasData(countries);
            builder.Entity<Customer>().HasData(customers);
            builder.Entity<Employee>().HasData(employees);
            builder.Entity<Shipment>().HasData(shipments);
            builder.Entity<Parcel>().HasData(parcels);
            builder.Entity<Status>().HasData(statuses);
            builder.Entity<Warehouse>().HasData(warehouses);

            base.OnModelCreating(builder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.EnableSensitiveDataLogging();
        }
    }
}
