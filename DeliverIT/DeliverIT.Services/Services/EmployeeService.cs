﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    
    public class EmployeeService : IEmployeeService
    {
        /// <summary>
        /// Db and warehouse service fields 
        /// </summary>
        private readonly DeliverITContext context;

        /// <summary>
        /// Ctor with db and warehouse service injections
        /// </summary>
        /// <param name="context"></param>
        /// <param name="warehouseService"></param>
        public EmployeeService(DeliverITContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Get employee by id
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        public EmployeeDTO Get(int id)
        {
            var employee = this.context.Employees
                .Where(em => !em.IsDeleted)
                .Include(em => em.Warehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .FirstOrDefault(em => em.EmployeeId == id)
                ?? throw new NonExistentId("employee", id);

            EmployeeDTO result = new EmployeeDTO(employee);

            return result;
        }
        
        /// <summary>
        /// Method to get employee by search criteria
        /// </summary>
        /// <param name="firstNameQr">First name criteria</param>
        /// <param name="lastNameQr">Last name criteria</param>
        /// <param name="emailQr">Email criteria</param>
        /// <returns></returns>
        public IEnumerable<EmployeeDTO> Get(string firstNameQr, 
            string lastNameQr, 
            string emailQr)
        {
            IQueryable<Employee> employees = this.context.Employees
                .Where(em => !em.IsDeleted)
                .Include(em => em.Warehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .AsQueryable();

            if (emailQr != null)
            {
                employees = employees.Where(em => em.Email.Contains(emailQr));

            }

            if (firstNameQr != null)
            {
                employees = employees.Where(em => em.FirstName.Contains(firstNameQr));
            }

            if (lastNameQr != null)
            {
                employees = employees.Where(em => em.LastName.Contains(lastNameQr));
            }

            var result = employees.ToList()
                .Select(e => new EmployeeDTO(e));

            return result;
        }

        /// <summary>
        /// Get employee by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns></returns>
        public EmployeeDTO GetByEmail(string email)
        {
            var employee =  this.context.Employees
                .Where(em => !em.IsDeleted)
                .Include(em => em.Warehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .FirstOrDefault(em => em.Email == email)
                ?? throw new NonExistentName("email", email);

            EmployeeDTO result = new EmployeeDTO(employee);

            return result;
        }

        /// <summary>
        /// Method to create employee
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <returns></returns>
        public EmployeeDTO Post(string firstName, 
            string lastName, 
            string email, 
            int warehouseId)
        {
            var employee = new Employee
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                WarehouseId = warehouseId,
                Warehouse = this.context.Warehouses
                .FirstOrDefault(w => w.WarehouseId == warehouseId)
            };

            var newEmployee = context.Employees.Add(employee);
            context.SaveChanges();

            EmployeeDTO result = new EmployeeDTO(employee);

            return result;
        }

        /// <summary>
        /// Method to delete employee
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                var employee = this.context.Employees
                    .FirstOrDefault(e => e.EmployeeId == id);
                employee.IsDeleted = true;

                context.SaveChanges();
                return true;
            }
            catch (NonExistentId)
            {
                return false;
            }
           

            
        }

        /// <summary>
        /// Put method to update employee
        /// </summary>
        /// <param name="id">Employee Id</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <returns></returns>
        public EmployeeDTO Put(int id, 
            string firstName, 
            string lastName, 
            string email, 
            int warehouseId)
        {
            var employee = this.context.Employees
                    .FirstOrDefault(e => e.EmployeeId == id);

            if (firstName != null)
                employee.FirstName = firstName;

            if (lastName != null)
                employee.LastName = lastName;

            if (email != null)
                employee.Email = email;

            if (warehouseId != 0 && warehouseId != employee.WarehouseId)
                employee.WarehouseId = warehouseId;

            context.SaveChanges();

            EmployeeDTO result = new EmployeeDTO(employee);

            return result;
        }

        /// <summary>
        /// Method to search employee by keyword
        /// </summary>
        /// <param name="seeker">Seeker keyword</param>
        /// <returns></returns>
        public IEnumerable<EmployeeDTO> Search(string seeker)
        {
            var employees = this.context.Employees.
                Where(em => !em.IsDeleted).
                Where(em => em.Email.Contains(seeker) || em.FirstName.Contains(seeker) || em.LastName.Contains(seeker))
                .Include(em => em.Warehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .ToList()
                .Select(e => new EmployeeDTO(e));

            return employees;
        }
    }
}
