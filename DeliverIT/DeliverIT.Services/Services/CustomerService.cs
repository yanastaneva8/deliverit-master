﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Services
{
    public class CustomerService : ICustomerService
    {
        /// <summary>
        /// Db and warehouse service field
        /// </summary>
        private readonly DeliverITContext context;

        
        /// <summary>
        /// Ctor with db and warehouse service injection
        /// </summary>
        /// <param name="deliverITContext"></param>
        /// <param name="warehouseService"></param>
        public CustomerService(DeliverITContext deliverITContext)
        {
            this.context = deliverITContext;
        }

        /// <summary>
        /// Get customer by id
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        public CustomerDTO Get(int id)
        {
            var customer = this.context.Customers
                .Where(cu => cu.IsDeleted == false)
                .Include(cu => cu.DeliveryWarehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .Include(cu => cu.Parcels)
                .FirstOrDefault(cu => cu.CustomerId == id)
                ?? throw new NonExistentId("customer", id);

            CustomerDTO result = new CustomerDTO(customer);

            return result;
        }

        /// <summary>
        /// Get customer by email
        /// </summary>
        /// <param name="email">Email</param>
        /// <returns>Customer</returns>
        public CustomerDTO GetByEmail(string email)
        {
            var customer = this.context.Customers
                 .Where(cu => cu.IsDeleted == false)
                .Include(cu => cu.DeliveryWarehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .FirstOrDefault(cu => cu.Email == email)
                ?? throw new NonExistentName("email", email);

            CustomerDTO result = new CustomerDTO(customer);

            return result;
        }


        /// <summary>
        /// Get customer by search criteria
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <returns></returns>
        public IEnumerable<CustomerDTO> Get(string firstName,
            string lastName,
            string email)
        {
            IQueryable<Customer> customers = this.context.Customers
                .Where(cu => !cu.IsDeleted)
                .Include(cu => cu.DeliveryWarehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .AsQueryable();

            if (email != null)
            {
                customers = customers.Where(cu => cu.Email.Contains(email));

            }

            if (firstName != null)
            {
                customers = customers.Where(cu => cu.FirstName.Contains(firstName));
            }

            if (lastName != null)
            {
                customers = customers.Where(cu => cu.LastName.Contains(lastName));
            }

            var result =  customers.ToList().Select(c => new CustomerDTO(c));
            return result;

        }

        /// <summary>
        /// Gets the number of all customers in the database.
        /// </summary>
        /// <returns>int : the number of customers</returns>
        public int GetTotalNumber()
        {
            return this.context.Customers.Count();
        }

        /// <summary>
        /// Create new customer
        /// </summary>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="deliveryWarehouseId">Delivery warehouse Id</param>
        /// <param name="roleId">Role Id</param>
        /// <returns></returns>
        public CustomerDTO Post(string firstName,
            string lastName,
            string email,
            int deliveryWarehouseId)
        {
            var customer = new Customer
            {
                FirstName = firstName,
                LastName = lastName,
                Email = email,
                DeliveryWarehouseId = deliveryWarehouseId,
                DeliveryWarehouse = this.context.Warehouses
                .FirstOrDefault(w => w.WarehouseId == deliveryWarehouseId)
            };

            var newCustomer = context.Customers.Add(customer);
            context.SaveChanges();

            CustomerDTO result = new CustomerDTO(customer);

            return result;
        }

        /// <summary>
        /// Delete customer
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            try
            {
                var customer = this.context.Customers
                    .FirstOrDefault(c => c.CustomerId == id);
                customer.IsDeleted = true;
                context.SaveChanges();
                return true;
            }
            catch (NonExistentId)
            {
                return false;
            }
            
        }

        /// <summary>
        /// Update customer properties
        /// </summary>
        /// <param name="id">Customer Id</param>
        /// <param name="firstName">First name</param>
        /// <param name="lastName">Last name</param>
        /// <param name="email">Email</param>
        /// <param name="deliveryWarehouseId">Delivery warehouse Id</param>
        /// <returns></returns>
        public CustomerDTO Put(int id,
            string firstName,
            string lastName,
            string email,
            int deliveryWarehouseId)
        {
            var customer = this.context.Customers
                .FirstOrDefault(c => c.CustomerId == id);

            if (firstName != null)
                customer.FirstName = firstName;

            if (lastName != null)
                customer.LastName = lastName;

            if (email != null)
                customer.Email = email;

            if (deliveryWarehouseId != 0 && deliveryWarehouseId != customer.DeliveryWarehouseId)
                customer.DeliveryWarehouseId = deliveryWarehouseId;

            context.SaveChanges();
            CustomerDTO result = new CustomerDTO(customer);

            return result;
        }

        /// <summary>
        /// Get parcels of customer by criteria
        /// </summary>
        /// <param name="customerId">Customer Id</param>
        /// <param name="deliveryStatusCriteria">Status to filter by</param>
        /// <returns></returns>
        public IQueryable<Parcel> GetParcelsOfCustomerByCriteria(int customerId, string deliveryStatusCriteria)
        {
            IQueryable<Parcel> parcels = this.context.Parcels
                .Where(p => p.CustomerId == customerId)
                .Include(p=>p.Customer);

            return deliveryStatusCriteria switch
            {
                "preparing" => parcels.Where(p => p.Shipment.StatusId == 1),

                "ontheway" => parcels.Where(p => p.Shipment.StatusId == 2),

                "completed" => parcels.Where(p => p.Shipment.StatusId == 3 && p.IsCollected == false),

                "collected" => parcels.Where(p => p.IsCollected == true),

                null=>parcels,

                _ => throw new InvalidQueryCriteria($"{deliveryStatusCriteria}", "parcel")
            };
        }

        /// <summary>
        /// Method to search customer by keyword
        /// </summary>
        /// <param name="seeker">Seeker keyword</param>
        /// <returns></returns>
        public IEnumerable<CustomerDTO> Search(string seeker)
        {
            var customers = this.context.Customers
                .Where(cu => !cu.IsDeleted)
                .Where(cu => cu.Email.Contains(seeker) || cu.FirstName.Contains(seeker) || cu.LastName.Contains(seeker))
                .Include(cu => cu.DeliveryWarehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country);
            var result = customers.ToList().Select(c => new CustomerDTO(c));

            return result;
        }

        


        public Customer DbGetOne(int id)
        {
            var customer = this.context.Customers
                .Where(cu => cu.IsDeleted == false)
                .Include(cu => cu.DeliveryWarehouse)
                    .ThenInclude(wh => wh.Address)
                        .ThenInclude(a => a.City)
                            .ThenInclude(ci => ci.Country)
                .Include(cu => cu.Parcels)
                .FirstOrDefault(cu => cu.CustomerId == id)
                ?? throw new NonExistentId("customer", id);
            return customer;
        }


    }
}
