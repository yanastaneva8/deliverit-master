﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Exceptions;
using DeliverIT.Services.Contracts;
using DeliverIT.Services.DTOs;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Services
{
    public class WarehouseService : IWarehouseService
    {
        /// <summary>
        /// Db and address service fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly IAddressService addressService;


        /// <summary>
        /// Ctor with db and address service injection
        /// </summary>
        /// <param name="context"></param>
        /// <param name="addressService"></param>
        public WarehouseService(DeliverITContext context,
            IAddressService addressService)
        {
            this.context = context;
            this.addressService = addressService;
        }

        /// <summary>
        /// Get warehouse by Id
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public WarehouseDTO Get(int id)
        {
            var warehouse = this.DbGetOne(id);

            WarehouseDTO result = new WarehouseDTO(warehouse);

            return result;
        }

        /// <summary>
        /// Get next shipment to arrive in warehouse
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public Shipment GetNextShipment(int id)
        {
            var shipment = context.Shipments
                .Where(s => s.ArrivalWarehouseId == id)
                .Where(s => s.StatusId == 2)
                .OrderByDescending(s => s.ArrivalDate)
                .FirstOrDefault()
                ?? throw new NoPendingItems("shipments");

            return shipment;
        }

        /// <summary>
        /// Get warehouses
        /// </summary>
        /// <returns></returns>
        public IEnumerable<WarehouseDTO> Get()
        {
            var warehouses = this.DbGetAll()
                .Select(w => new WarehouseDTO(w));

            return warehouses;
        }



        /// <summary>
        /// Create new warehouse
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <returns></returns>
        public WarehouseDTO Post(string cityName,
            string countryName,
            string streetName)
        {
            var warehouse = new Warehouse();

            var address = addressService.Post(cityName, countryName, streetName);
            warehouse.AddressId = this.context.Addresses
                .FirstOrDefault(a => a.StreetName == streetName)
                .AddressId;
            warehouse.Address = this.context.Addresses
                .FirstOrDefault(a => a.StreetName == streetName);

            var newWarehouse = this.context.Warehouses.Add(warehouse);
            context.SaveChanges();

            WarehouseDTO result = new WarehouseDTO(warehouse);

            return result;
        }

        /// <summary>
        /// Update warehouse properties
        /// </summary>
        /// <param name="warehouseId">Warehouse Id</param>
        /// <param name="newCountry">New country</param>
        /// <param name="newCity">New city</param>
        /// <param name="newStreetName">New street name</param>
        /// <returns></returns>
        public WarehouseDTO Put(int warehouseId,
            string newCountry,
            string newCity,
            string newStreetName)
        {
            var warehouse = this.context.Warehouses
                .FirstOrDefault(w => w.WarehouseId == warehouseId);

            var newAdrress = addressService.Post(newCity, newCountry, newStreetName);

            warehouse.Address = this.context.Addresses
                .FirstOrDefault(a => a.StreetName == newStreetName);

            context.SaveChanges();

            WarehouseDTO result = new WarehouseDTO(warehouse);

            return result;
        }

        /// <summary>
        /// Delete warehouse
        /// </summary>
        /// <param name="id">Warehouse Id</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            var warehouse = this.context.Warehouses
                .FirstOrDefault(w => w.WarehouseId == id);

            if (warehouse == null)
                return false;

            warehouse.IsDeleted = true;

            context.SaveChanges();
            return true;
        }


        public Warehouse DbGetOne(int id)
        {
            var warehouse = context.Warehouses
                .Where(w => !w.IsDeleted)
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .FirstOrDefault(w => w.WarehouseId == id)
                ?? throw new NonExistentId("warehouse", id);
            return warehouse;
        }

        public IEnumerable<Warehouse> DbGetAll()
        {
            var warehouses = context.Warehouses
                .Where(w => !w.IsDeleted)
                .Include(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(c => c.Country);
            return warehouses;

        }
    }
}
