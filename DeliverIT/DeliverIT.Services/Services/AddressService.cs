﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using DeliverIT.Services.Contracts;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Services
{
    public class AddressService : IAddressService
    {
        /// <summary>
        /// Db, city and country services fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly ICityService cityService;
        private readonly ICountryService countryService;


        /// <summary>
        /// Ctor with db, city & country services injections
        /// </summary>
        /// <param name="deliverITContext"></param>
        /// <param name="cityService"></param>
        /// <param name="countryService"></param>
        public AddressService(DeliverITContext deliverITContext, 
            ICityService cityService, 
            ICountryService countryService)
        {
            this.context = deliverITContext;
            this.cityService = cityService;
            this.countryService = countryService;
        }

        /// <summary>
        /// Get all addresses
        /// </summary>
        /// <returns>IEnumerable of addresses</returns>
        public IEnumerable<AddressDTO> Get()
        {
            var addresses = this.DbGetAll()
                .Select(a => new AddressDTO(a));

            return addresses;
        }

        /// <summary>
        /// Get address by id
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <returns>Address</returns>
        public AddressDTO Get(int id)
        {
            var address = this.DbGetOne(id);

            AddressDTO result = new AddressDTO(address);

            return result;
        }

        /// <summary>
        /// Post method to create address
        /// </summary>
        /// <param name="cityName">City name</param>
        /// <param name="countryName">Country name</param>
        /// <param name="streetName">Street name</param>
        /// <returns>Address</returns>
        public AddressDTO Post(string cityName, 
            string countryName, 
            string streetName)
        {
            var address = new Address();

            var country = this.countryService.Get(countryName);
            address.CountryId = this.context.Countries
                .FirstOrDefault(c => c.Name == countryName)
                .CountryId;
            address.Country = this.context.Countries
                .FirstOrDefault(c => c.Name == countryName);

            var city = this.cityService.Get(cityName)
                ?? this.cityService.Post(cityName, countryName);

            address.CityId = this.context.Cities
                .FirstOrDefault(c => c.Name == cityName)
                .CityId;

            address.City = this.context.Cities
                .FirstOrDefault(c => c.Name == cityName);

            address.StreetName = streetName;

            var newAddress = context.Addresses.Add(address);
            context.SaveChanges();

            AddressDTO result = new AddressDTO(address);

            return result;
        }
        
        /// <summary>
        /// Put method to update address
        /// </summary>
        /// <param name="id">Address Id</param>
        /// <param name="countryName">Country name</param>
        /// <param name="cityName">City name</param>
        /// <param name="streetName">Street name</param>
        /// <returns>Address</returns>
        public AddressDTO Put(int id, 
            string countryName, 
            string cityName, 
            string streetName)
        {
            var address = this.DbGetOne(id);

            if (countryName != null)
            {
                var country = countryService.Get(countryName);
                address.CountryId = this.context.Countries
                    .FirstOrDefault(c => c.Name == countryName)
                    .CountryId;
                address.Country = this.context.Countries
                    .FirstOrDefault(c => c.Name == countryName);
            }

            if (cityName != null)
            {
                var city = cityService.Get(cityName)
                ?? cityService.Post(cityName, countryName);
                address.CityId = this.context.Cities
                .FirstOrDefault(c => c.Name == cityName)
                .CityId;

                address.City = this.context.Cities
                .FirstOrDefault(c => c.Name == cityName);
            }

            if (address.City.CountryID != address.Country.CountryId)
                throw new UnsynchronizedIds(address.Country.Name, address.City.Name, address.Country.CountryId);

            if (streetName != null)
            {
                address.StreetName = streetName;
            }

            context.SaveChanges();
            AddressDTO result = new AddressDTO(address);

            return result;

        }

        public Address DbGetOne(int id)
        {
            var address = this.context.Addresses
                 .Include(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(a => a.Country)
                .FirstOrDefault(a => a.AddressId == id)
                ?? throw new NonExistentId("address", id);

            return address;
        }

        public IEnumerable<Address> DbGetAll()
        {
            var addresses = this.context.Addresses
                 .Include(a => a.City)
                    .ThenInclude(c => c.Country)
                .Include(a => a.Country);

            return addresses;
        }

    }
}