﻿using DeliverIT.Data;
using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using DeliverIT.Exceptions;
using Microsoft.EntityFrameworkCore;
using DeliverIT.Services.Contracts;
using System.Globalization;
using DeliverIT.Services.DTOs;

namespace DeliverIT.Services.Services
{
    public class ShipmentService : IShipmentService
    {
        /// <summary>
        /// Database context and warehouse and parcel fields
        /// </summary>
        private readonly DeliverITContext context;
        private readonly IWarehouseService warehouseService;


        /// <summary>
        /// Ctor with db, warehouse and parcel service injections
        /// </summary>
        /// <param name="context"></param>
        public ShipmentService(DeliverITContext context,
            IWarehouseService warehouseService)
        {
            this.context = context;
            this.warehouseService = warehouseService;
        }

        /// <summary>
        /// Get shipment by id method
        /// </summary>
        /// <param name="id">Shipment id</param>
        /// <returns>Shipment</returns>
        public ShipmentDTO Get(int id)
        {
            var shipment = this.DbGetOne(id);

            return new ShipmentDTO(shipment);
        }

        /// <summary>
        /// Get all shipments
        /// </summary>
        /// <param name="customerId">Customer Id to filter shipments by customer</param>
        /// <returns>IQueryable of shipments</returns>
        public IQueryable<ShipmentDTO> GetMany(int? customerId)
        {
            var shipments = this.DbGetAll();

            if (customerId != default)
            {
                shipments = shipments.Where(s => s.Parcels.Any(p => p.CustomerId == customerId));
            }

            return shipments.Select(sh=>new ShipmentDTO(sh)).AsQueryable();
        }


        /// <summary>
        /// Create shipment method
        /// </summary>
        /// <param name="departureDateString">String with departure date</param>
        /// <param name="arrivalDateString">String with arrival date</param>
        /// <param name="departureWarehouseId">Departure warehouse Id</param>
        /// <param name="arrivalWarehouseId">Arrival warehouse Id</param>
        /// <returns>Shipment</returns>
        public ShipmentDTO Post(string departureDateString,
            string arrivalDateString,
            int departureWarehouseId,
            int arrivalWarehouseId)
        {
            var shipment = new Shipment();

            // Convert string departureDateString to DateTime
            var departureDate = DateTime.ParseExact(departureDateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            // Convert string arrivalDateString to DateTime
            var arrivalDate = DateTime.ParseExact(arrivalDateString, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            // Query db for departure warehouse
            var departureWarehouse = this.warehouseService.DbGetAll()
                .FirstOrDefault(w => w.WarehouseId == departureWarehouseId);

            // Query db for arrival warehouse
            var arrivalWarehouse = this.warehouseService.DbGetAll()
                .FirstOrDefault(w => w.WarehouseId == arrivalWarehouseId);


            // Set properties
            shipment.DepartureDate = departureDate;
            shipment.ArrivalDate = arrivalDate;
            shipment.ArrivalWarehouse = arrivalWarehouse;
            shipment.ArrivalWarehouseId = arrivalWarehouse.WarehouseId;
            shipment.DepartureWarehouse = departureWarehouse;
            shipment.DepartureWarehouseId = departureWarehouse.WarehouseId;
            shipment.StatusId = 1; // Default status Preparing
            shipment.Status = context.Statuses.FirstOrDefault(s => s.StatusId == shipment.StatusId)
                ?? throw new NonExistentId("status", shipment.StatusId);


            // Save changes
            var newShipment = this.context.Shipments.Add(shipment);
            context.SaveChanges();

            return new ShipmentDTO(newShipment.Entity);
        }

        /// <summary>
        /// Advance status method
        /// </summary>
        /// <param name="shipmentId">Shipment id</param>
        public void AdvanceStatus(int shipmentId)
        {
            // Get shipment
            var shipment = this.DbGetOne(shipmentId);

            // Advance statusId if not final
            if (shipment.StatusId < 3)
            {
                shipment.StatusId++;
            }
            else
            {
                throw new StatusReached("Completed");
            }

            // Advance status based on the advanced statusId
            shipment.Status = context.Statuses
                .First(s => s.StatusId == shipment.StatusId);

            context.SaveChanges();
        }

        /// <summary>
        /// Revert status method
        /// </summary>
        /// <param name="shipmentId">Shipment id</param>
        public void RevertStatus(int shipmentId)
        {
            // Get shipment
            var shipment = this.DbGetOne(shipmentId);

            // Revert statusId if not initial or final
            if (shipment.StatusId > 1 && shipment.StatusId != 3)
            {
                shipment.StatusId--;
            }
            else
            {
                throw new StatusReached("Completed");
            }

            // Revert status based on the reversed statusId
            shipment.Status = context.Statuses
                .First(s => s.StatusId == shipment.StatusId);

            context.SaveChanges();
        }

        /// <summary>
        /// Dispatch shipment method
        /// </summary>
        /// <param name="shipmentId">Shipment id</param>
        /// <param name="travelTimeHrs">Travel time in hours</param>
        public void DispatchShipment(int shipmentId,
            int travelTimeHrs)
        {
            // Get shipment
            var shipment = this.DbGetOne(shipmentId);

            // If shipment is empty, do not dispatch
            if (shipment.Parcels.Count == 0)
                throw new CannotBeEmpty("Shipment");

            // Advance status of shipment
            if (shipment.StatusId == 1)
            {
                this.AdvanceStatus(shipmentId);
            }
            else
            {
                throw new CannotUpdateStatus("on the way", "preparing");
            }

            // Departure date is datetime when this method is called
            shipment.DepartureDate = DateTime.Now;

            // Arrival date is departure date + travel time in hours
            shipment.ArrivalDate = shipment.DepartureDate.AddHours(travelTimeHrs);


            // Save changes in db
            context.SaveChanges();
        }

        /// <summary>
        /// Receive shipment method
        /// </summary>
        /// <param name="shipmentId">Shipment Id</param>
        public void ReceiveShipment(int shipmentId)
        {
            // Get shipment
            var shipment = this.DbGetOne(shipmentId);

            // Check if status is on the way, if not - throw exception
            if (shipment.StatusId < 2)
            {
                throw new CannotUpdateStatus("completed", "on the way");
            }

            // Arrival date is now
            shipment.ArrivalDate = DateTime.Now;

            // Set status to final status
            this.AdvanceStatus(shipmentId);

            // Save changes in db
            context.SaveChanges();
        }

        /// <summary>
        /// Method to add parcels to shipment
        /// </summary>
        /// <param name="shipmentId">Shipment Id</param>
        /// <param name="parcelIds">Int array of parcel Ids</param>
        /// <returns>List of parcels</returns>
        public List<ParcelDTO> AddParcelsToShipment(int shipmentId, int[] parcelIds)
        {
            // Query db for shipment with shipmentId
            var shipment = this.DbGetOne(shipmentId);

            // Initialize list of parcels
            List<Parcel> parcels = new List<Parcel>();

            // Check if parcels with specified Ids exist in db
            foreach (var id in parcelIds)
            {
                var parcel = this.context.Parcels
                .Where(p => !p.IsDeleted)
                .Include(p => p.Customer)
                    .ThenInclude(c => c.DeliveryWarehouse)
                    .ThenInclude(d => d.Address)
                .Include(p => p.ArrivalWarehouse)
                    .ThenInclude(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .Include(p => p.DepartureWarehouse)
                    .ThenInclude(w => w.Address)
                    .ThenInclude(a => a.City)
                    .ThenInclude(a => a.Country)
                .Include(p => p.Category)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.ArrivalWarehouse)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.DepartureWarehouse)
                .Include(p => p.Shipment)
                    .ThenInclude(s => s.Status)
                 .FirstOrDefault(p => p.ParcelId == id)
                    ?? throw new NonExistentId("parcel", id);

                // Add parcel to shipment
                parcel.ShipmentId = shipmentId;
                parcel.Shipment = shipment;
                parcels.Add(parcel);
            }


            shipment.Parcels.Concat(parcels).Distinct();

            context.SaveChanges();
            return parcels.Select(pa=>new ParcelDTO(pa)).ToList();
        }


        /// <summary>
        /// Delete shipment
        /// </summary>
        /// <param name="id">Shipment Id</param>
        /// <returns></returns>
        public bool Delete(int id)
        {
            var shipment = this.DbGetOne(id);
            if (shipment == null)
                return false;

            shipment.IsDeleted = true;

            context.SaveChanges();
            return true;
        }



        public Shipment DbGetOne(int id)
        {
            var shipment = context.Shipments
                .Include(s => s.DepartureWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.City)
                 .Include(s => s.DepartureWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.City)
                 .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Status)
                .FirstOrDefault(s => s.ShipmentId == id && !s.IsDeleted)
                ?? throw new NonExistentId("shipment", id);

            return shipment;
        }

        public IEnumerable<Shipment> DbGetAll()
        {
            var shipments = this.context.Shipments
                .Where(s => !s.IsDeleted)
                .Include(s=>s.DepartureWarehouse)
                    .ThenInclude(s=>s.Address)
                        .ThenInclude(s=>s.City)
                 .Include(s => s.DepartureWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.City)
                 .Include(s => s.ArrivalWarehouse)
                    .ThenInclude(s => s.Address)
                        .ThenInclude(s => s.Country)
                .Include(s => s.Parcels)
                    .ThenInclude(p => p.Customer)
                .Include(s => s.Status)
                .AsQueryable();

            return shipments;
        }




    }
}

