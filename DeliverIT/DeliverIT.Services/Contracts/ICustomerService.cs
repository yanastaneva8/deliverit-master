﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface ICustomerService
    {
        CustomerDTO Get(int id);
        IEnumerable<CustomerDTO> Get(string firstNameQr, string lastNameQr, string emailQr);
        CustomerDTO GetByEmail(string email);
        int GetTotalNumber();
        CustomerDTO Post(string firstName, string lastName, string email, int deliveryWarehouseId);
        bool Delete(int id);
        CustomerDTO Put(int id, string firstName, string lastName, string email, int deliveryWarehouseId);
        IQueryable<Parcel> GetParcelsOfCustomerByCriteria(int customerId, string deliveryStatusCriteria);
        IEnumerable<CustomerDTO> Search(string seeker);

        public Customer DbGetOne(int id);
    }
}
