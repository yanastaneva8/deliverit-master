﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IAddressService
    {
        IEnumerable<AddressDTO> Get();

        AddressDTO Get(int id);
        AddressDTO Post(string cityName, string countryName, string streetName);
        AddressDTO Put(int id, string countryName, string cityName, string streetName);
    }
}
