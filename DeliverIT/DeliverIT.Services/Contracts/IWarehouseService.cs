﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IWarehouseService
    {
        WarehouseDTO Get(int id);

        IEnumerable<WarehouseDTO> Get();

        Shipment GetNextShipment(int id);

        WarehouseDTO Post(string cityName, string countryName, string streetName);

        public WarehouseDTO Put(int warehouseId, string newCountry, string newCity, string newStreetName);

        bool Delete(int id);

        public Warehouse DbGetOne(int id);

        public IEnumerable<Warehouse> DbGetAll();

    }
}
