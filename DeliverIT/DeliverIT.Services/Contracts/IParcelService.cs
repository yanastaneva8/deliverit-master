﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IParcelService
    {
        public IEnumerable<ParcelDTO> Get(
           double weight,
           int customerId,
           int arrivalWarehouseId,
           int departureWarehouseId,
           int categoryId,
           string sortingCriteria,
           string sortingOrder);
        public ParcelDTO Get(int id);
        public ParcelDTO Post(int customerId,
            int departureWarehouseId,
            int arrivalWarehouseId,
            double weight,
            int categoryId,
            int? shipmentId);
        public ParcelDTO Put(int parcelId,
            int newArrivalWarehouseId,
            int newDepartureWarehouseId,
            int newShipmentId);
        public Status GetStatusOfShipmentHoldingParcel(int parcelId, int customerId);
        public bool Delete(int id);

        public IEnumerable<Parcel> DbGetAll();
        public Parcel DbGetOne(int id);

    }
}
