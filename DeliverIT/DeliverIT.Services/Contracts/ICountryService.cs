﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;


namespace DeliverIT.Services.Contracts
{
    public interface ICountryService
    {
        CountryDTO Get(int id);

        CountryDTO Get(string countryName);

        IEnumerable<CountryDTO> GetAll();
    }
}
