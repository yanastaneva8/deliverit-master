﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;


namespace DeliverIT.Services.Contracts
{
    public interface ICityService
    {
        CityDTO Get(int id);

        CityDTO Get(string cityName);

        IEnumerable<CityDTO> Get();

        CityDTO Post(string cityName, string countryName);
    }
}
