﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.Contracts
{
    public interface IEmployeeService
    {
        EmployeeDTO Get(int id);
        IEnumerable<EmployeeDTO> Get(string firstNameQr, string lastNameQr, string emailQr);
        EmployeeDTO Post(string firstName, string lastName, string email, int warehouseId);
        bool Delete(int id);
        EmployeeDTO Put(int id, string firstName, string lastName, string email, int warehouseId);
        IEnumerable<EmployeeDTO> Search(string seeker);
        EmployeeDTO GetByEmail(string email);
    }
}
