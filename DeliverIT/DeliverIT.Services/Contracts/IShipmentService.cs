﻿using DeliverIT.Data.Models;
using DeliverIT.Services.DTOs;
using System.Collections.Generic;
using System.Linq;

namespace DeliverIT.Services.Contracts
{
    public interface IShipmentService
    {
        public ShipmentDTO Get(int id);

        public IQueryable<ShipmentDTO> GetMany(int? customerId);

        public ShipmentDTO Post(string departureDateString,
            string arrivalDateString,
            int departureWarehouseId,
            int arrivalWarehouseId);

        public void AdvanceStatus(int shipmentId);

        public void RevertStatus(int shipmentId);

        public void DispatchShipment(int shipmentId,
            int travelTimeHrs);

        public void ReceiveShipment(int shipmentId);

        public List<ParcelDTO> AddParcelsToShipment(int shipmentId, int[] parcelIds);

        public bool Delete(int id);

        public Shipment DbGetOne(int id);

        public IEnumerable<Shipment> DbGetAll();

    }
}
