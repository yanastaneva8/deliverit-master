﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class EmployeeDTO
    {
        public EmployeeDTO(Employee employee)
        {
            this.Name = employee.FirstName + " " + employee.LastName;
            this.Email = employee.Email;
            this.Warehouse = employee.WarehouseId + ": " +
                employee.Warehouse.Address.StreetName + ", " +
                employee.Warehouse.Address.City.Name + ", " +
                employee.Warehouse.Address.Country.Name;
            this.EmployeeId = employee.EmployeeId;
        }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Warehouse { get; set; }
        public int EmployeeId { get; set; }
    }
}
