﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class CityDTO
    {
        public CityDTO(City city)
        {
            this.Name = city.Name;
            this.Country = city.Country.Name;
        }
        public string Name { get; set; }
        public string Country { get; set; }

    }
}
