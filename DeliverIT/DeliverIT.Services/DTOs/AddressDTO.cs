﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class AddressDTO
    {
        public AddressDTO(Address address)
        {
            this.StreetName = address.StreetName;
            this.City = address.City.Name;
            this.Country = address.Country.Name;
        }
        public string StreetName { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

    }
}
