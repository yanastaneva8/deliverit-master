﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class CountryDTO
    {
        public CountryDTO(Country country)
        {
            this.Name = country.Name;
        }
        public string Name { get; set; }
    }
}
