﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class WarehouseDTO
    {
        public WarehouseDTO(Warehouse warehouse)
        {
            this.Warehouse = warehouse.WarehouseId + ": " + 
                warehouse.Address.StreetName + ", " +
                warehouse.Address.City.Name + ", " +
                warehouse.Address.Country.Name;
        }
        public string Warehouse { get; set; }
    }
}
