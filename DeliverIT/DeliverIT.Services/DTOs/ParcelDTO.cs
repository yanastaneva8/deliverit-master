﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ParcelDTO
    {
        public ParcelDTO(Parcel parcel)
        {
            this.ParcelId = parcel.ParcelId;
            this.ShipmentId = parcel.ShipmentId;
            this.CustomerId = parcel.CustomerId;
            this.CustomerName = $"{parcel.Customer.FirstName} {parcel.Customer.LastName}";
            this.DepartureWarehouseId = parcel.DepartureWarehouseId;
            this.DepartureWarehouseAddress = new AddressDTO(parcel.DepartureWarehouse.Address);
            this.ArrivalWarehouseId = parcel.ArrivalWarehouseId;
            this.ArrivalWarehouseAddress = new AddressDTO(parcel.ArrivalWarehouse.Address);
            this.Weight = parcel.Weight;
            this.CategoryId = parcel.CategoryId;
            this.Category = parcel.Category.Name;
            this.ArrivalDate = DateTime.ParseExact(parcel.Shipment.ArrivalDate.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);
            this.IsCollected = parcel.IsCollected ? "Collected" : "Not collected";
        }

        public int ParcelId { get; set; }
        public int CustomerId { get; set; }
        public string CustomerName { get; set; }
        public int DepartureWarehouseId { get; set; }
        public AddressDTO DepartureWarehouseAddress { get; set; }
        public int ArrivalWarehouseId { get; set; }
        public AddressDTO ArrivalWarehouseAddress { get; set; }
        public double Weight { get; set; }
        public int CategoryId { get; set; }
        public string Category { get; set; } 
        public DateTime ArrivalDate { get; set; }
        public string IsCollected { get; set; }
        public int? ShipmentId { get; set; }
    }
}
