﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace DeliverIT.Services.DTOs
{
    public class CustomerDTO
    {
        public CustomerDTO(Customer customer)
        {
            this.Id = customer.CustomerId;
            this.Name = customer.FirstName + " " + customer.LastName;
            this.Email = customer.Email;
            this.Warehouse = customer.DeliveryWarehouseId + ": " +
                customer.DeliveryWarehouse.Address.StreetName + ", " +
                customer.DeliveryWarehouse.Address.City.Name + ", " +
                customer.DeliveryWarehouse.Address.Country.Name;
            this.Parcels = customer.Parcels.Select(p => p.ParcelId).ToList();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Warehouse { get; set; }
        public List<int> Parcels { get; set; }

    }
}
