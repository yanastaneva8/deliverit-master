﻿using DeliverIT.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DeliverIT.Services.DTOs
{
    public class ShipmentDTO
    {
        public ShipmentDTO(Shipment shipment)
        {
            this.ShipmentId = shipment.ShipmentId;
            this.DepartureDate = shipment.DepartureDate;
            this.ArrivalDate = shipment.ArrivalDate;
            this.Status = shipment.Status;
            this.DepartureWarehouse = new WarehouseDTO(shipment.DepartureWarehouse);
            this.ArrivalWarehouse = new WarehouseDTO(shipment.ArrivalWarehouse);
            this.ParcelsIds = shipment.Parcels != null ? shipment.Parcels.Select(pa => pa.ParcelId) : null;
            this.CustomersFor = shipment.Parcels != null ? shipment.Parcels.Select(pa => pa.CustomerId) : null;
        }

        public int ShipmentId { get; set; }
        public DateTime DepartureDate { get; set; }
        public DateTime ArrivalDate { get; set; }
        public Status Status { get; set; }
        public WarehouseDTO DepartureWarehouse { get; set; }
        public WarehouseDTO ArrivalWarehouse { get; set; }
        public IEnumerable<int> ParcelsIds { get; set; }
        public IEnumerable<int> CustomersFor { get; set; }

    }
}
