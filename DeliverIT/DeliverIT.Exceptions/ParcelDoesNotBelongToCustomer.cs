﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ParcelDoesNotBelongToCustomer : Exception
    {
        public ParcelDoesNotBelongToCustomer()
        {
        }
        public ParcelDoesNotBelongToCustomer(int parcelId, int customerId)
            : base(String.Format("Customer with id {0} does not own parcel with id {1}", customerId, parcelId))
        {
        }
    }
}
