﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class CannotBeNegative : Exception
    {
        public CannotBeNegative()
        {

        }

        public CannotBeNegative(string item, string type)
            : base(String.Format("{0} must be a positive {1}", item, type))
        {

        }
    }
}
