﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class ParcelNotAssignedToShipment : Exception
    {
        public ParcelNotAssignedToShipment()
        {
        }
        public ParcelNotAssignedToShipment(int id)
            : base(String.Format("Parcel with id {0} not assigned to any shipment", id))
        {
        }
    }
}
