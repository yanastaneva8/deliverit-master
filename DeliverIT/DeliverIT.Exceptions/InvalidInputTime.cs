﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class InvalidInputTime : Exception
    {
        public InvalidInputTime()
        {

        }

        public InvalidInputTime(DateTime inputTime)
            : base(String.Format("Input time {0} ivalid.", inputTime))
        {

        }
    }
}
