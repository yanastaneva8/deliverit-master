﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Services.Services
{
    public class CannotBeEmpty : Exception
    {
        public CannotBeEmpty()
        {

        }

        public CannotBeEmpty(string type)
            : base(String.Format("{0} cannot be empty", type))
        {

        }
    }
}
