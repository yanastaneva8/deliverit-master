﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DeliverIT.Exceptions
{
    public class UnsynchronizedIds : Exception
    {
        public UnsynchronizedIds()
        {

        }

        public UnsynchronizedIds(string parentObjectName, string childObjectName, int parentObjectId)
            : base(String.Format("Object {0} has id {2}, but {1} has parent object of the same type with different id", 
                parentObjectName, 
                childObjectName, 
                parentObjectId))
        {

        }
    }
}
